# LUMIFOR
## Calcul et optimisation de la durée d'ensoleillement au sein d'une ouverture forestière

Version : 1.1  
Langage : Python 3.x  

Financement : **Office National des Forêts - Pôle RDI de Chambéry** (*laurent.malabeux@onf.fr*)  
![ONF](./img/onf_logo.gif?raw=true)

Conception et développement : **Sylvain DUPIRE, SylvaLab - 2023** (*sylvain.dupire@inrae.fr*)   
![SylvaLab](./img/logo_sylvalab.png?raw=true)

---
### Contexte

En forêt de montagne, l'**étagement par bouquets ou groupes d'arbres** est un objectif optimal. Cette **structuration** peut être atteinte en créant des **ouvertures pour rajeunir le peuplement**. Mais comment favoriser un **mélange d'essences dont les besoins en lumière et en chaleur sont différents** ? Simplement en jouant sur la **taille, la forme et l'orientation des ouvertures** selon le contexte (pente, exposition, hauteur du peuplement...). 

Néanmoins, une mise en œuvre non réfléchie de cette sylviculture peut engendrer des **conséquences négatives si l’étendue des ouvertures est trop importante** : perte du microclimat forestier, explosion de la végétation concurrente, reptation accrue du manteau neigeux...

### LUMIFOR : objectifs et fonctionnalitée

Lumifor est une application permettant d’**analyser la durée d’éclairement direct au sol au sein d’une ouverture forestière en contexte de pente**. A travers trois outils distincts (voir ci-dessous), le gestionnaire forestier a la possibilité de mieux comprendre cette notion d’éclairement direct et donc d’améliorer la mise en œuvre volontaire d’une ouverture au sein d’un peuplement forestier. 

![SylvaLab](./img/course_soleil.png?raw=true)

Les outils présents dans Lumifor lui permettront notamment d’obtenir des indications sur : 

- les formes d’ouverture à privilégier 
- les orientations les plus favorables 
- les dimensions optimales 
- la durée horaire d’éclairement direct au sein d’une trouée notamment au mois de Juin 

### Précautions d'utilisation de LUMIFOR
La meilleure compréhension de la notion d’éclairement direct n’est qu’un **outil d’aide à la décision** pour le gestionnaire forestier lors de la phase de diagnostic préalable à toutes interventions sylvicoles. En fonction des divers compartiments analysés (aléas naturels, station forestière, peuplement forestier, mode d’exploitation…), il est donc nécessaire d’adapter les indications fournies par Lumifor à ce contexte.


Tags = ___Lumière___, ___forêt___, ___montagne___, ___ouverture___, ___MNT___, ___trouée___, ___création___, ___renouvellement___, ___régénération___, ___sylviculture___, ___Lidar___

---  
### Trois outils différents et complémentaires

![SylvaLab](./img/basic_calc.png?raw=true) **Calcul de la durée d'ensoleillement direct pour une ouverture définie**

Cet outil permet d’obtenir des informations sur l’ensoleillement au sein d’une ouverture forestière dont les paramètres influençant la course du soleil sont fixés par l'utilisateur.  
Nous sommes donc la situation d’analyse d’une ouverture existante ou de simulation de divers projets d’ouverture pour comprendre l’impact de différents facteurs sur l’ensoleillement direct au sein de ces ouvertures forestières.  

![SylvaLab](./img/optim.png?raw=true) **Optimisation de la forme, la taille et l'orientation d'une ouverture à créer**

Cet outil permet de connaître les ouvertures dont la forme, la taille et l’orientation optimisent la proportion de surface la plus importante dans une plage horaire donnée d’ensoleillement direct au mois de juin. Le fait de contraindre la plage horaire d’ensoleillement direct offre logiquement la possibilité de favoriser le rajeunissement de certaines essences forestières sans pour autant amener trop de lumière ce qui pourrait avoir des effets néfastes.  
Nous sommes donc dans la situation où avant un martelage, le gestionnaire forestier doit donner des consignes pour la réalisation d’ouverture forestière adaptée à un contexte d’application.  


![SylvaLab](./img/calc_terrain.png?raw=true)**Calculer la durée d'ensoleillement direct au sein d'une ouverture sur un terrain réel**

Cet outil vous d’obtenir des informations sur l’ensoleillement au sein d’une ouverture forestière en utilisant des données s’approchant d’une réalité de terrain grâce à l’utilisation d’un MNT et un MNS issus de données LiDAR.  
Nous sommes donc la situation d’analyse détaillée de l’impact de création d’une ouverture dans un cas réel.  


  
---  


__Licence Lumifor :__

    Copyright (C) 2023 by Sylvain DUPIRE.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see https://www.gnu.org/licenses/



