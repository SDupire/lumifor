# -*- coding: utf-8 -*-
"""
Software: LUMIFOR
File: Lumifor_1_function.py
Copyright (C) Sylvain DUPIRE 2023
Authors: Sylvain DUPIRE
Contact: sylvain.dupire@inrae.fr
Version: 1.1
Date: 2023/04/27
License :  GNU-GPL V3
"""
import ephem 
import os,shutil
import datetime
import numpy as np
from calendar import monthrange
import pytz,timezonefinder,math
from osgeo import gdal,ogr,osr
from shapely import speedups
speedups.disable()
from shapely.geometry import Polygon,LineString,Point
import matplotlib.pyplot as plt
from scipy import ndimage,spatial
from matplotlib.patches import Patch, Ellipse,Rectangle
from matplotlib.colors import ListedColormap
import matplotlib.colors as colors
from matplotlib_scalebar.scalebar import ScaleBar
from matplotlib import collections  as mc
import matplotlib as mpl
import calc_duration as cd 
from math import cos,atan,radians,sqrt,sin
import openpyxl
from openpyxl import load_workbook
from openpyxl import Workbook
from openpyxl.drawing.image import Image
from openpyxl.styles import Alignment
from pyproj import Transformer

###############################################################################
### Function
###############################################################################
def LoadRspace():
    GRspace =  os.path.expanduser("~/.lumifor/")
    SC_Rspace = GRspace+"SpecCalc/"
    Opt_Rspace = GRspace+"Optim/"
    User_Rspace = GRspace+"UserParam/"
    Ter_Rspace = GRspace+"Terrain/"
    Temp_Rspace = GRspace+"Temp/"

    if not os.path.exists(User_Rspace):
        try:
            os.mkdir(GRspace)
            os.mkdir(User_Rspace)            
        except: pass  
        
        Params = np.array([45.5090,6.1854,1000])
        DWdir = ""
        f = open(User_Rspace+"Default_Result_Directory.txt","w")
        lines = f.write('')
        f.close()
        np.save(User_Rspace+"Param.npy",Params)
        
    else:
        f = open(User_Rspace+"Default_Result_Directory.txt","r")
        lines = f.readlines()
        try:
            DWdir = lines[0]
        except:
            DWdir = ""
        f.close()
        Params = np.float32(np.load(User_Rspace+"Param.npy"))
         
    lW = [SC_Rspace,Opt_Rspace,Ter_Rspace,Temp_Rspace]
    
    for D in lW:
        if os.path.exists(D):
            shutil.rmtree(D)
    
        try:os.mkdir(D)
        except: pass   
    return GRspace,Params,DWdir

def load_Database():
    Lumifor_Database = np.load('ressource/Lumifor_Database.npz')   
    DB_form=np.uint8(Lumifor_Database["form"])
    DB_Dtm_az=np.uint8(Lumifor_Database["Dtm_az"])
    DB_Dtm_sl=np.uint8(Lumifor_Database["Dtm_sl"])
    DB_Stand_H=np.uint8(Lumifor_Database["Stand_H"])
    DB_dim1=np.uint16(Lumifor_Database["dim1"])
    DB_dim2=np.uint16(Lumifor_Database["dim2"])
    DB_f_az=np.int8(Lumifor_Database["f_az"])
    DB_Lat=np.uint16( Lumifor_Database["Lat"]),
    DB_Lpente = np.uint8(Lumifor_Database["Lpente"])
    DB_Surf=np.uint16(Lumifor_Database["Surf"])
    Radiations=np.int16(Lumifor_Database["Radiations"])
    Clouds=np.int16(Lumifor_Database["Clouds"])
    
    return DB_form,DB_Dtm_az,DB_Dtm_sl,DB_Stand_H,DB_dim1,DB_dim2, \
        DB_f_az,DB_Lat,DB_Lpente,DB_Surf,Radiations,Clouds

def create_res_dir(Rspace,Dtm_az,Dtm_sl,Elevation,Stand_H,form,
                   dim1,dim2,f_az,Lat,Lon):          
    
    Rspace2 = Rspace+"/Lat"+str(Lat)+"_Lon"+str(Lon)+'_Alt'+str(Elevation) 
    Rspace2 += "_Exp"+str(int(Dtm_az*20/18))+"_Pt"+str(Dtm_sl)
    Rspace2 += '_H'+str(int(Stand_H))+"m_"
    June_ok=False
    Month_ok=False
    Day_ok=False
    Rad_ok=False
    
    if int(dim1)==dim1:
        str_dim1 = str(int(dim1))
    else:
        str_dim1 = str(round(dim1,2))
    if int(dim2)==dim2:
        str_dim2 = str(int(dim2))
    else:
        str_dim2 = str(round(dim2,2))
                     
    if form=='rectangle':
        if dim1!=dim2:
            Rspace2 += "RECTANGLE_"+str_dim1+"H_"+str_dim2+"H_"+"Ori"+str(f_az)
        else:
            Rspace2 += "CARRE_"+str_dim1+"H_"+"Ori"+str(f_az)
    if form=='ellipse':         
        if dim1!=dim2:
            Rspace2 += "ELLIPSE_"+str_dim1+"H_"+str_dim2+"H_"+"Ori"+str(f_az)
        else:
            Rspace2 += "CERCLE_"+str_dim1+"H"
    
    if os.path.exists(Rspace2): 
        for file in os.listdir(Rspace2):
            if file.startswith("Duree_moy"):
                June_ok=True 
            elif file.startswith("Duree_21"):
                Month_ok=True
            elif file.startswith("Enso"):
                Day_ok=True 
            elif file.startswith("Rayon"):
                Rad_ok=True            
    try:os.mkdir(Rspace2)
    except:pass   
    return Rspace2+'/',June_ok,Month_ok,Day_ok,Rad_ok

def create_res_dir_opti(Rspace,Dtm_az,Dtm_sl,Elevation,Stand_H,Lat,Lon,
                        Plage_min,Plage_max ,Durmax ,SurfDmax,LProtec):          
    
    Rspace2 = Rspace+"/Lat"+str(Lat)+"_Lon"+str(Lon)+'_Alt'+str(Elevation) 
    Rspace2 += "_Exp"+str(Dtm_az)+"_Pt"+str(Dtm_sl)
    Rspace2 += '_H'+str(int(Stand_H))+"_"+str(Plage_min)+" "+str(Plage_max)
    Rspace2 += '_'+str(Durmax)+"_"+str(SurfDmax)+"_"+str(LProtec)
    exist=0
           
    if os.path.exists(Rspace2): 
        for file in os.listdir(Rspace2):
            if file.startswith("Config"):
                exist+=1 
            elif file.startswith("Rayon"):
                exist+=10 
    if exist==14:
        exist=True
    else:
        exist=False
        try:os.mkdir(Rspace2)
        except:pass   
    return Rspace2+'/',exist



def complete_resfile(Rspace,Dtm_az,Dtm_sl,Elevation,Stand_H,form,
                     dim1,dim2,f_az,Lat,Lon,borne_sup,
                     Sun_duration_min,nbdays,Shape):
    
    filename = Rspace+"Recapitulatif_simulations.csv"
    #header
    header = "Forme;Exposition (grad);Pente (%);Altitude (m);Lat (°);Lon (°);"
    header += "Hauteur peuplement (m);Dim 1;Dim 2;Orientation (grad);"
    header += "Longueur terrain dans l'axe de plus grande pente (m);"
    header += "0 h/jour;Moins de "+str(borne_sup[0])+" h/jour;"
    prev = "Entre "+str(borne_sup[0])+ ' et '
    for duree in borne_sup[1:]:
        header += prev + str(duree)+" h/jour;"
        prev = "Entre "+str(duree)+ ' et '
    header += "Plus de "+str(duree)+" h/jour\n"
    
    #Lpente
    Lpente= str(round(get_Lpente(Stand_H,form,dim1,dim2,f_az,Dtm_sl),1))
    
    #dimension
    if int(dim1)==dim1:
        str_dim1 = str(int(dim1))
    else:
        str_dim1 = str(round(dim1,2))
    if int(dim2)==dim2:
        str_dim2 = str(int(dim2))
    else:
        str_dim2 = str(round(dim2,2))    
        
    #form
    if form=='rectangle':
        if dim1!=dim2:
            For = "Rectangle;"
            str_dims = str_dim1+";"+str_dim2+";"+str(f_az)+";"
        else:
            For = "Carre;"
            str_dims = str_dim1+";;"+str(f_az)+";"
    else:
        if dim1!=dim2:
            For = "Ellipse;"
            str_dims = str_dim1+";"+str_dim2+";"+str(f_az)+";"
        else:
            For = "Cercle;"
            str_dims = str_dim1+";;;"

    txt = For+str(int(Dtm_az*20/18))+';'+str(Dtm_sl)+";"+str(Elevation)+";"
    txt += str(round(Lat,5))+";"+str(round(Lon,5))+";"+str(Stand_H)+";"+str_dims
    txt += Lpente+";"
    
    #Surface
    Surface = calc_surface_min(Sun_duration_min,Shape,nbdays)
    tab_plage = calc_surface_plage(Surface,borne_sup)
    
    for i in tab_plage[:,2]:
        txt += str(round(i,1))+";"
    txt+="\n"
    
    if os.path.exists(filename):
        f = open(filename,"a+")
        f.write(txt)
        f.close()
    else:
        f = open(filename,"w")
        f.write(header+txt)
        f.close()      


def calc_sun_posi(Month,Tlaps,Lat,Lon,Elevation):  
    obs=ephem.Observer()
    obs.lat = str(Lat)
    obs.lon = str(Lon)  
    obs.elev = Elevation
    tf = timezonefinder.TimezoneFinder()  
    try:
        timezone_str = tf.certain_timezone_at(lat=Lat, lng=Lon)
        timezone = pytz.timezone(timezone_str)
    except:
        timezone_str = tf.certain_timezone_at(lat=45, lng=0)
        timezone = pytz.timezone(timezone_str)
    now = datetime.datetime.now(timezone)
    Year = now.year    
    ff,nbdays=monthrange(Year, Month)
    Sun_posi = np.ones((int(nbdays*24*60/Tlaps+1.5),2),dtype=np.int32)*-1
    i=0
    for day in range(1,nbdays+1):
        d1 = datetime.datetime(Year,Month,day,00,00,00)#Localtime
        obs.date = d1 - timezone.utcoffset(d1)
        try:
            sunrise=obs.next_rising(ephem.Sun()).datetime() 
            sunset=obs.next_setting(ephem.Sun()).datetime() 
        except:
            sunrise=datetime.datetime(Year,Month,day,00,00,00)
            sunset=datetime.datetime(Year,Month,day,23,59,00)
        t = sunrise
        while t<sunset:            
            obs.date = t
            sun=ephem.Sun(obs)
            Sun_posi[i,1] = abs(int(math.tan(sun.alt)*10000+0.5))
            Sun_posi[i,0] = int(math.degrees(sun.az)+0.5) 
            t+=datetime.timedelta(minutes=Tlaps)  
            i+=1
    Sun_posi=Sun_posi[:i]
    u,counts = np.unique(Sun_posi,axis=0,return_counts=True)
    Sun_posi = np.zeros((u.shape[0],3),dtype=np.int32)
    Sun_posi[:,0:2]=u
    Sun_posi[:,2]=counts*Tlaps      
    az_list,indmin = np.unique(Sun_posi[:,0],return_index=True)
    Az_val = np.zeros((az_list.shape[0],2),dtype=np.int32)
    Az_val[:,0] = az_list
    Az_val[:,1] = indmin
    
    return Sun_posi,Az_val,nbdays

def get_solar_ephem(Tlaps,Lat,Lon,Elevation):
    obs=ephem.Observer()
    obs.lat = str(Lat)
    obs.lon = str(Lon)  
    obs.elev = Elevation
    tf = timezonefinder.TimezoneFinder()  
    try:
        timezone_str = tf.certain_timezone_at(lat=Lat, lng=Lon)
        timezone = pytz.timezone(timezone_str)
    except:
        timezone_str = tf.certain_timezone_at(lat=45, lng=0)
        timezone = pytz.timezone(timezone_str)
    now = datetime.datetime.now(timezone)
    Year = now.year 
    Sun_posi = np.ones((int(365*24*60/Tlaps+1.5),4),dtype=np.float32)*-1
    i=0  
    Day=0
    for Month in range(1,13):
        ff,nbdays=monthrange(Year, Month)
        for day in range(1,nbdays+1): 
            Day+=1
            d1 = datetime.datetime(Year,Month,day,00,00,00)#Localtime
            obs.date = d1 - timezone.utcoffset(d1)
            try:
                sunrise=obs.next_rising(ephem.Sun()).datetime() 
                sunset=obs.next_setting(ephem.Sun()).datetime() 
            except:
                sunrise=datetime.datetime(Year,Month,day,00,00,00)
                sunset=datetime.datetime(Year,Month,day,23,59,00)
            t = sunrise
            while t<sunset:            
                obs.date = t
                sun=ephem.Sun(obs)
                Sun_posi[i,0] = sun.az
                Sun_posi[i,1] = sun.alt
                Sun_posi[i,2] = Month
                Sun_posi[i,3] = Day                
                t+=datetime.timedelta(minutes=Tlaps)  
                i+=1
            
    return Sun_posi[:i]
    
def calc_radiation_year(Sun_posi,Elevation,Dtm_az,Dtm_sl):     
    Slope = atan(Dtm_sl/100.)
    Azi = radians(Dtm_az)
    pratio = ((288-0.0065*Elevation)/288)**5.256
    Day = Sun_posi[:,3]
    Rout = 1367*(1+0.034*np.cos(360*Day/365)) 
    Sun_elev = Sun_posi[:,1]
    Sun_az = Sun_posi[:,0]
    M = pratio*(np.sqrt(1229+np.power(614*np.sin(Sun_elev),2))-614*np.sin(Sun_elev))
    cosi = np.cos(Sun_elev)*np.sin(Slope)*np.cos(Sun_az-Azi)+np.sin(Sun_elev)*np.cos(Slope)
    
    Rdir = np.maximum(0,cosi*Rout*np.power(0.6,M))
    Rdiff = np.maximum(0,np.sin(Sun_elev)*Rout*(0.271-0.294*np.power(0.6,M)))
    Rref = np.maximum(0,0.2*1367*(0.271+0.706*np.power(0.6,M))*np.sin(Sun_elev)*(np.sin(Slope/2)*np.sin(Slope/2)))
    
    Rdir *= 60#J/min/m²
    RdifRef = (Rdiff+Rref)*60#J/min/m²    
    
    Rad = np.zeros((13,),dtype=np.int16)
    for i in range(1,13):
        tp = Sun_posi[:,2]==i
        Rad[i-1]=int(np.sum(Rdir[tp])/1000000  +0.5)  
    Rad[i]= int(np.sum(RdifRef)/1000000  +0.5)  
    
    return  Rad

def calc_sun_posi_day(Lat,Lon,Elevation,Tlaps=30,Month=6,Day=21):  
    obs=ephem.Observer()
    obs.lat = str(Lat)
    obs.lon = str(Lon)  
    obs.elev = Elevation
    tf = timezonefinder.TimezoneFinder()
    try:
        timezone_str = tf.certain_timezone_at(lat=Lat, lng=Lon)
        timezone = pytz.timezone(timezone_str)
    except:
        timezone_str = tf.certain_timezone_at(lat=45, lng=0)
        timezone = pytz.timezone(timezone_str)
    now = datetime.datetime.now(timezone)
    Year = now.year  
    Sun_posi = np.ones((int(24*60/Tlaps+1.5),4),dtype=np.float32)*-1
    #Alt Azimut H Min
    i=0   
    d1 = datetime.datetime(Year,Month,Day,00,00,00)#Localtime
    obs.date = d1 - timezone.utcoffset(d1)
    try:
        sunrise=obs.next_rising(ephem.Sun()).datetime() 
        sunset=obs.next_setting(ephem.Sun()).datetime() 
    except:
        sunrise=datetime.datetime(Year,Month,Day,00,00,00)
        sunset=datetime.datetime(Year,Month,Day,23,59,00)  
    sunset+=datetime.timedelta(minutes=Tlaps) 
    t = sunrise    
    if t.minute>30 :
        t=t.replace(hour=t.hour+1,minute=0,second=0)  
    elif t.minute!=0:
        t=t.replace(minute=30,second=0)     
    while t<sunset:            
        obs.date = t
        tlocal = t+timezone.utcoffset(t)
        sun=ephem.Sun(obs)
        Sun_posi[i,0] = math.degrees(sun.az)
        Sun_posi[i,1] = abs(math.tan(sun.alt))        
        Sun_posi[i,2] = tlocal.hour
        Sun_posi[i,3] = tlocal.minute  
        t+=datetime.timedelta(minutes=Tlaps)  
        i+=1
        
    Sun_posi=Sun_posi[:i]
         
    return Sun_posi


def get_sun_posi_21Month(Tlaps,Lat,Lon,Elevation):  
    obs=ephem.Observer()
    obs.lat = str(Lat)
    obs.lon = str(Lon)  
    obs.elev = Elevation
    tf = timezonefinder.TimezoneFinder()
    try:
        timezone_str = tf.certain_timezone_at(lat=Lat, lng=Lon)
        timezone = pytz.timezone(timezone_str)
    except:
        timezone_str = tf.certain_timezone_at(lat=45, lng=0)
        timezone = pytz.timezone(timezone_str)
    now = datetime.datetime.now(timezone)
    Year = now.year  
    Sun_posi = np.ones((int(12*24*60/Tlaps+1.5),14),dtype=np.int32)*-1  
    Sun_posi[:,2:]=0
    i=0   
    for Month in range(1,13):
        d1 = datetime.datetime(Year,Month,21,00,00,00)#Localtime
        obs.date = d1 - timezone.utcoffset(d1)
        try:
            sunrise=obs.next_rising(ephem.Sun()).datetime() 
            sunset=obs.next_setting(ephem.Sun()).datetime() 
        except:
            sunrise=datetime.datetime(Year,Month,21,00,00,00)
            sunset=datetime.datetime(Year,Month,21,23,59,00)
        t = sunrise
        while t<sunset:            
            obs.date = t
            sun=ephem.Sun(obs)
            Sun_posi[i,1]=abs(int(math.tan(sun.alt)*10000+0.5))
            Sun_posi[i,0]= int(math.degrees(sun.az)+0.5) 
            Sun_posi[i,Month+1] = Tlaps
            t+=datetime.timedelta(minutes=Tlaps)  
            i+=1
    Sun_posi=Sun_posi[:i]
    SolDur = Sun_posi[:,2:]*Tlaps
    SolPosi = Sun_posi[:,0:2]
    u,index,counts = np.unique(SolPosi,axis=0,return_counts=True,return_index=True)
    Sun_posi = np.zeros((u.shape[0],14),dtype=np.int32)
    for i,posi in enumerate(u):
        Sun_posi[i,0:2] = posi 
        if counts[i]==1:
            Sun_posi[i,2:]=SolDur[index[i]]
        else:            
            inds = np.argwhere(((SolPosi[:,0]==posi[0])*(SolPosi[:,1]==posi[1]))>0)[:,0]
            for j in inds:
                Sun_posi[i,2:]+=SolDur[j]
            
    az_list,indmin = np.unique(Sun_posi[:,0],return_index=True)
    Az_val = np.zeros((az_list.shape[0],2),dtype=np.int32)
    Az_val[:,0] = az_list
    Az_val[:,1] = indmin   
    return Sun_posi,Az_val
  
def build_inclined_plane(Csize,Dtm_cote,Dtm_sl,Elevation):
    Csize*=1.0
    nrows,ncols = int(Dtm_cote/Csize+0.5),int(Dtm_cote/Csize+0.5)
    if nrows%2==0:
        nrows+=1
        ncols+=1
    Dtm = np.zeros((nrows,ncols),dtype=np.float32)
    ymid = int(nrows/2)
    for i in range(nrows):
        Dtm[i]=(i-ymid)*Dtm_sl/100.+Elevation
    return Dtm,nrows,ncols

def conv_az_to_polar(az):
    return (360-(az-90))%360
    
def draw_rectangle(r_dim1,r_dim2,rAz,Dtm,Csize,Temp_Rspace):
    rLo,rLa=max(r_dim1,r_dim2),min(r_dim1,r_dim2)
    nrows,ncols=Dtm.shape
    ymid,xmid = int(nrows/2),int(ncols/2)   
    
    rect = Rectangle((ymid,xmid), rLa, rLo, -rAz) 
    vertices = rect.get_verts()     # get the vertices from the ellipse object
    
    xmid2 = 0.5*(np.min(vertices[:,0])+np.max(vertices[:,0]))
    ymid2 = 0.5*(np.min(vertices[:,1])+np.max(vertices[:,1]))
    
    corx = xmid-xmid2   
    cory = ymid-ymid2 
        
    vertices[:,0]+=corx
    vertices[:,1]+=cory
    
    rectangle = Polygon(vertices)
    
    rect = Rectangle((ymid,xmid), rLa, rLo, rAz) 
    vertices = rect.get_verts()  
    xmid2 = 0.5*(np.min(vertices[:,0])+np.max(vertices[:,0]))
    ymid2 = 0.5*(np.min(vertices[:,1])+np.max(vertices[:,1]))
    
    corx = xmid-xmid2   
    cory = ymid-ymid2 
        
    vertices[:,0]+=corx
    vertices[:,1]+=cory
        
    driver = ogr.GetDriverByName("GeoJSON")
    Filename = Temp_Rspace+'rectangle.geojson'
    if os.path.exists(Filename):driver.DeleteDataSource(Filename)
    target_ds = driver.CreateDataSource(Filename)
    layerName = os.path.splitext(os.path.split(Filename)[1])[0]
    layer = target_ds.CreateLayer(layerName, None, ogr.wkbPolygon)
    layerDefinition = layer.GetLayerDefn()
    new_field = ogr.FieldDefn('Rast', ogr.OFTInteger)
    layer.CreateField(new_field)
           
    feature = ogr.Feature(layerDefinition)    
    feature.SetFID(0)
    feature.SetField('Rast',1)   
    geom = ogr.CreateGeometryFromWkb(rectangle.wkb)
    feature.SetGeometry(geom)
    layer.CreateFeature(feature)   
    feature.Destroy()
    # Cleanup
    target_ds.Destroy()
    
    source_ds = ogr.Open(Filename)
    source_layer = source_ds.GetLayer()
    source_srs = source_layer.GetSpatialRef()
    source_layer.GetExtent()
    xmin, ymax = 0,nrows
    
    target_ds = gdal.GetDriverByName('MEM').Create('', int(ncols), int(nrows), 1, gdal.GDT_Int32)
    maskvalue = 1  
    xres=float(Csize)
    yres=float(Csize)
    geotransform=(xmin,xres,0,ymax,0, -yres) 
    target_ds.SetGeoTransform(geotransform)
    if source_srs:
        # Make the target raster have the same projection as the source
        target_ds.SetProjection(source_srs.ExportToWkt())
    else:
        # Source has no projection (needs GDAL >= 1.7.0 to work)
        target_ds.SetProjection('LOCAL_CS["arbitrary"]')
    # Rasterize
    attribute_name="Rast"
    err = gdal.RasterizeLayer(target_ds, [maskvalue], source_layer,options=["ATTRIBUTE="+attribute_name,"ALL_TOUCHED=FALSE"])
    if err != 0:
        target_ds.FlushCache()
        source_ds.Destroy()
        raise Exception("error rasterizing layer: %s" % err)
    else:
        target_ds.FlushCache()
        source_ds.Destroy()
        mask_arr = target_ds.GetRasterBand(1).ReadAsArray()    
        if os.path.exists(Filename):driver.DeleteDataSource(Filename)
        return np.uint8(mask_arr),vertices

def draw_ellipse(e_dim1,e_dim2,Dtm,rAz,Csize,Temp_Rspace):   
    
    rLo,rLa=max(e_dim1,e_dim2),min(e_dim1,e_dim2)
    nrows,ncols=Dtm.shape
    ymid,xmid = int(nrows/2),int(ncols/2)   

    ellipse = Ellipse((xmid, ymid), rLa, rLo, -rAz) 
    vertices = ellipse.get_verts()     # get the vertices from the ellipse object
    ellipse = Polygon(vertices)
    
    ellipse2 = Ellipse((xmid, ymid), rLa, rLo, rAz) 
    vertices = ellipse2.get_verts()  
        
    driver = ogr.GetDriverByName("GeoJSON")
    Filename = Temp_Rspace+'ellipse.geojson'
    if os.path.exists(Filename):driver.DeleteDataSource(Filename)
    target_ds = driver.CreateDataSource(Filename)
    layerName = os.path.splitext(os.path.split(Filename)[1])[0]
    layer = target_ds.CreateLayer(layerName, None, ogr.wkbPolygon)
    layerDefinition = layer.GetLayerDefn()
    new_field = ogr.FieldDefn('Rast', ogr.OFTInteger)
    layer.CreateField(new_field)
           
    feature = ogr.Feature(layerDefinition)    
    feature.SetFID(0)
    feature.SetField('Rast',1)   
    geom = ogr.CreateGeometryFromWkb(ellipse.wkb)
    feature.SetGeometry(geom)
    layer.CreateFeature(feature)   
    feature.Destroy()
    # Cleanup
    target_ds.Destroy()
    
    source_ds = ogr.Open(Filename)
    source_layer = source_ds.GetLayer()
    source_srs = source_layer.GetSpatialRef()
    source_layer.GetExtent()
    xmin, ymax = 0,nrows
    
    target_ds = gdal.GetDriverByName('MEM').Create('', int(ncols), int(nrows), 1, gdal.GDT_Int32)
    maskvalue = 1  
    xres=float(Csize)
    yres=float(Csize)
    geotransform=(xmin,xres,0,ymax,0, -yres) 
    target_ds.SetGeoTransform(geotransform)
    if source_srs:
        # Make the target raster have the same projection as the source
        target_ds.SetProjection(source_srs.ExportToWkt())
    else:
        # Source has no projection (needs GDAL >= 1.7.0 to work)
        target_ds.SetProjection('LOCAL_CS["arbitrary"]')
    # Rasterize
    attribute_name="Rast"
    err = gdal.RasterizeLayer(target_ds, [maskvalue], source_layer,options=["ATTRIBUTE="+attribute_name,"ALL_TOUCHED=FALSE"])
    if err != 0:
        target_ds.FlushCache()
        source_ds.Destroy()
        raise Exception("error rasterizing layer: %s" % err)
    else:
        target_ds.FlushCache()
        source_ds.Destroy()
        mask_arr = target_ds.GetRasterBand(1).ReadAsArray()    
        if os.path.exists(Filename):driver.DeleteDataSource(Filename)
        return np.uint8(mask_arr),vertices
    

def calculate_azimut(x1,y1,x2,y2):
    """
    Calculate the azimuth between two points from their coordinates
    """
    DX = x2-x1
    DY = y2-y1
    Deuc = math.sqrt(DX**2+DY**2)
    if x2>x1:Fact=1
    else:Fact=-1
    Angle = math.degrees(math.acos(DY/Deuc))
    Angle *=Fact
    return Angle%360    

def calc_radiation(Sun_posi,Dtm_az,Dtm_sl,Elevation,Month,Lat,Lon):
    Day = 0
    for m in range(1,Month):
        ff,nbdays=monthrange(2020, m)
        Day+=nbdays
    ff,nbdays=monthrange(2020, Month)
    Day+=0.5*nbdays
    
    Sun_posi2 = np.zeros((Sun_posi.shape[0],Sun_posi.shape[1]+1),dtype=np.float32)
    Sun_posi2[:,:Sun_posi.shape[1]]=Sun_posi
    Sun_posi2[:,1]=Sun_posi2[:,1]/10000.
    
    Slope = atan(Dtm_sl/100.)
    Azi = radians(Dtm_az)
        
    #Incoming solar flux from piedallu
    Rout = 1367*(1+0.034*cos(360*Day/365))
    pratio = ((288-0.0065*Elevation)/288)**5.256
    
    
    Sun_elev = np.arctan(Sun_posi2[:,1])
    Sun_az = np.radians(Sun_posi2[:,0])
    
    M = pratio*(np.sqrt(1229+np.power(614*np.sin(Sun_elev),2))-614*np.sin(Sun_elev))
    cosi = np.cos(Sun_elev)*np.sin(Slope)*np.cos(Sun_az-Azi)+np.sin(Sun_elev)*np.cos(Slope)
      
      
    Rdir = np.maximum(0,cosi*Rout*np.power(0.6,M))
    Rdiff = np.maximum(0,np.sin(Sun_elev)*Rout*(0.271-0.294*np.power(0.6,M)))
    Rref = np.maximum(0,0.2*1367*(0.271+0.706*np.power(0.6,M))*np.sin(Sun_elev)*(np.sin(Slope/2)*np.sin(Slope/2)))

    Sun_posi2[:,3] = Rdir*(Sun_posi2[:,2]*60)#J/min/m²
    Rmin = (Rdiff+Rref)*(Sun_posi2[:,2]*60) #J/min/m²      
        
    # Kc = (1-0.75*(cloud_cover/100.)**3.4)
    # Sun_posi2[:,4] = Sun_posi2[:,3]*Kc#in J/min/m² 
    return (np.sum(Sun_posi2[:,3])+np.sum(Rmin))/1000000  
  
def visualiser(Raster):    
    # new_magma = plt.cm.get_cmap('magma',5)
    plt.imshow(Raster)
    plt.colorbar()
    plt.show()
    
def plot_Sun_Duration(Raster,Csize,vertices,f_dim1,f_dim2,Dtm_az,Dtm_sl,Stand_H,
                      Dtm_masked,str_title,form,fig_name=None,borne_sup=[1,2,4,6]):     
    
    #build shape of the forest gap     
    lines=[]  
    for i, pt in enumerate(vertices[:-1]):       
        pt2 = vertices[i+1]        
        lines.append([(pt[0],pt[1]),(pt2[0],pt2[1])])
    
    lc = mc.LineCollection(lines, colors='black', linewidths=2)
    
    #zoom 
    inds = np.argwhere(Raster>=0)
    ymin = int(min(np.min(inds[:,0]),np.min(inds[:,1]))-10/Csize)
    xmin = ymin
    ymax = int(max(np.max(inds[:,0]),np.max(inds[:,1]))+10/Csize+0.5)
    xmax=ymax    
    
    X, Y = np.meshgrid(np.arange(xmin, xmax, dtype=np.int), 
                       np.arange(ymin, ymax, dtype=np.int))
    
    Z = Dtm_masked[ymin:ymax,xmin:xmax]    
    contourval = int((np.max(Z)-np.min(Z))/5+0.5)
        
    #Colors used
    cmap = ListedColormap(["gainsboro","#0072B2",
                           "#56B4E9","#009E73",
                           "#F0E442","#E69F00",
                           "#CC79A7"])    
     
    # Define a normalization from values -> colors
    nbcol = len(borne_sup)+2
    bins = np.empty((nbcol,))
    bins[0:2]=-0.5,0.000000000001
    i=2
    while i<bins.shape[0]:
        bins[i]=borne_sup[i-2]
        i+=1
    
    inds = np.digitize(Raster, bins)    
    bound_col=[0]
    for i in range(nbcol+1):
        bound_col.append(i+0.5)        
    norm = colors.BoundaryNorm(bound_col, nbcol+1)
    
    #Text on clear cut dimension  
    str_Tarea="Hauteur du peuplement : "+str(Stand_H)+ "m\n"
    
    # Give dimension accroding to StandH
    if int(f_dim1/Stand_H)==f_dim1/Stand_H:
        st1 = str(int(f_dim1/Stand_H))+"H"
    else:
        st1 = str(round(f_dim1/Stand_H,2))+"H"
    if int(f_dim2/Stand_H)==f_dim2/Stand_H:
        st2 = str(int(f_dim2/Stand_H))+"H"
    else:
        st2 = str(round(f_dim2/Stand_H,2)) +"H"     
    ##################################################################
    if f_dim1 != f_dim2:
        str_Tarea+="Dimension de l'ouverture' : "+st1+ r'$ \times $'+st2+'\n'
    else:
        str_Tarea+="Dimension de l'ouverture : "+st1+ '\n'
    
    if form=='rectangle':
        surf = str(int(f_dim1*f_dim2+0.5))        
    else:
        surf = str(int(0.5*f_dim1*0.5*f_dim2*math.pi+0.5))
    
    str_Tarea+="Surface de l'ouverture : "+surf+ ' m\u00B2'
    
    #Legend title
    str_title_ld = r"$\bf{Durée}$"+" "+r"$\bf{d}$"+"'"
    str_title_ld +=  r"$\bf{ensoleillement}$"+" "
    str_title_ld +=  r"$\bf{direct}$" + " "  
    str_title_ld +=  r"$\bf{(\%}$" + " "+r"$\bf{de}$" + " "
    str_title_ld +=  r"$\bf{la}$" + " "+r"$\bf{surface)}$" 
    
    # Calc_area
    Tarea_norm = np.sum(inds>0)
    area_list = []
    for val in range(1,nbcol+1): 
        if val==1:
            txt = "0 h/jour : "
        elif val==2:
            txt = 'Moins de '+str(borne_sup[0])+" h/jour : "
        elif val>2 and val<nbcol:
            txt = 'Entre '+str(borne_sup[val-3])+' et '+str(borne_sup[val-2])+" h/jour : "
        else:
            txt = "Plus de "+str(borne_sup[val-3])+" h/jour : "        
        txt += str(int(np.sum(inds==val)/Tarea_norm*100+0.5))+ ' %'
        area_list.append(txt)
        
    #Start fig
    plt.ioff()
    fig, ax = plt.subplots(figsize=(11.14,7.9),dpi=100)
          
    ax.imshow(inds,cmap=cmap,norm=norm,interpolation=None)
    ##add contour
    if contourval>0:      
        ax.contour(X, Y, Z, contourval, colors='black', linestyles=':',linewidths=0.75,alpha=0.7)    
    
    #legend
    legend_labels = {"#0072B2": area_list[0], 
                     "#56B4E9": area_list[1], 
                     "#009E73": area_list[2],
                     "#F0E442": area_list[3],
                     "#E69F00": area_list[4],
                     "#CC79A7": area_list[5]}
    patches = [Patch(color=color, label=label)
               for color, label in legend_labels.items()]
    
    fig.legend(loc="lower center",handles=patches,
               borderaxespad=0.05,frameon=False,
               title=str_title_ld,ncol=2)
    plt.subplots_adjust(bottom=0.11)  
    ax.add_collection(lc)
    if Dtm_sl>0:
        #Slope arrow
        bbox_props = dict(boxstyle="rarrow", fc="#9a1010", ec="#9a1010", lw=2,alpha=0.8)
        ax.text(0.1, 0.90, "          ", ha="center", va="center",
                rotation=conv_az_to_polar(Dtm_az),
                size=15,color='white',transform=ax.transAxes,
                bbox=bbox_props) 
        ang = conv_az_to_polar(Dtm_az)
        if Dtm_az>180:
            ang -= 180
        ax.text(0.11, 0.90, " Pente ", ha="center", va="center",
                rotation=ang,
                size=15,color='white',transform=ax.transAxes)
        #slope value
        ax.text(0.24, 0.90, str(Dtm_sl)+' %', ha="center", va="center",
                size=15,color='#9a1010',transform=ax.transAxes)
    else:
        ax.text(0.15, 0.95,"Pente : "+ str(Dtm_sl)+' %', ha="center", va="center",
                size=15,color='#9a1010',transform=ax.transAxes)
    #North arrow
    bbox_props = dict(boxstyle="rarrow", fc=(0.7, 0.7, 0.7), ec="black", lw=2)
    ax.text(0.95, 0.92, "       ", ha="center", va="center",
            rotation=90,transform=ax.transAxes,
            size=15,
            bbox=bbox_props)
    ax.text(0.95, 0.92, " N ", ha="center", va="center",transform=ax.transAxes,           
            size=15)     
    plt.xlim(xmin, xmax)
    plt.ylim(ymax, ymin)
    ax.text(0.01, 0.01, str_Tarea, ha="left", va="bottom",            
            size=12,transform=ax.transAxes)    
    scalebar = ScaleBar(Csize,frameon=False,location="lower right")
    plt.gca().add_artist(scalebar)
    ax.set_axis_off()       
    plt.gca().set_aspect('equal', adjustable='box')
    plt.title(str_title, fontsize=20,fontweight='bold')
    if fig_name is not None:
        fig.savefig(fig_name, dpi=fig.dpi,bbox_inches='tight')
    #plt.show() 
    plt.close()
    
    
def plot_SunDirect(Raster,Csize,vertices,f_dim1,f_dim2,Dtm_az,Dtm_sl,Stand_H,
                   Dtm_masked,str_title,form,fig_name=None):     
         
    lines=[]  
    for i, pt in enumerate(vertices[:-1]):       
        pt2 = vertices[i+1]        
        lines.append([(pt[0],pt[1]),(pt2[0],pt2[1])])
    
    lc = mc.LineCollection(lines, colors='black', linewidths=2)
    
    #zoom  
    inds = np.argwhere(Raster>=0)
    ymin = int(min(np.min(inds[:,0]),np.min(inds[:,1]))-10/Csize)
    xmin = ymin
    ymax = int(max(np.max(inds[:,0]),np.max(inds[:,1]))+10/Csize+0.5)
    xmax=ymax      
    
    
    X, Y = np.meshgrid(np.arange(xmin, xmax, dtype=np.int), 
                       np.arange(ymin, ymax, dtype=np.int))
    
    Z = Dtm_masked[ymin:ymax,xmin:xmax]    
    contourval = int((np.max(Z)-np.min(Z))/5+0.5)
        
    #Colors used
    cmap = ListedColormap(["gainsboro","#0072B2","#E69F00"])    
     
    # Define a normalization from values -> colors
    nbcol = 3
    bins = np.empty((nbcol,))
    bins[0:3]=-0.5,0.000000000001,1
   
    
    inds = np.digitize(Raster, bins)    
    bound_col=[0]
    for i in range(nbcol+1):
        bound_col.append(i+0.5)        
    norm = colors.BoundaryNorm(bound_col, nbcol+1)
    
    #Text on clear cut dimension  
    str_Tarea="Hauteur du peuplement : "+str(Stand_H)+ "m\n"
      
    # give dimension according to StandH
    if int(f_dim1/Stand_H)==f_dim1/Stand_H:
        st1 = str(int(f_dim1/Stand_H))+"H"
    else:
        st1 = str(round(f_dim1/Stand_H,2))+"H"
    if int(f_dim2/Stand_H)==f_dim2/Stand_H:
        st2 = str(int(f_dim2/Stand_H))+"H"
    else:
        st2 = str(round(f_dim2/Stand_H,2)) +"H"     
    ##################################################################
    if f_dim1 != f_dim2:
        str_Tarea+="Dimension de l'ouverture : "+st1+ r'$ \times $'+st2+'\n'
    else:
        str_Tarea+="Dimension de l'ouverture : "+st1+ '\n'
    
    if form=='rectangle':
        surf = str(int(f_dim1*f_dim2+0.5))        
    else:
        surf = str(int(0.5*f_dim1*0.5*f_dim2*math.pi+0.5))
    
    str_Tarea+="Surface de l'ouverture : "+surf + ' m\u00B2'
    
    # Calc_area
    Tarea_norm = np.sum(inds>0)
    area_list = []
    txt = "A l'ombre : "    
    txt += str(round(np.sum(inds==1)/Tarea_norm*100,1))+ ' %'
    area_list.append(txt)
    txt = "Au soleil : "
    txt += str(round(np.sum(inds>1)/Tarea_norm*100,1))+ ' %'
    area_list.append(txt)
        
    #Start fig
    plt.ioff()
    fig, ax = plt.subplots(figsize=(11.14,7.9),dpi=100)
          
    ax.imshow(inds,cmap=cmap,norm=norm,interpolation=None)
    ##add contour
    if contourval>0:      
        ax.contour(X, Y, Z, contourval, colors='black', linestyles=':',linewidths=0.75,alpha=0.7)    
        
    #legend
    legend_labels = {"#0072B2": area_list[0], 
                     "#E69F00": area_list[1]}
    patches = [Patch(color=color, label=label)
               for color, label in legend_labels.items()]
    
    fig.legend(loc="lower center",handles=patches,
               borderaxespad=0.05,frameon=False,
               fontsize=15,title='',ncol=2)
    plt.subplots_adjust(bottom=0.11)  
    ax.add_collection(lc)
    if Dtm_sl>0:
        #Slope arrow
        bbox_props = dict(boxstyle="rarrow", fc="#9a1010", ec="#9a1010", lw=2,alpha=0.8)
        ax.text(0.1, 0.90, "          ", ha="center", va="center",
                rotation=conv_az_to_polar(Dtm_az),
                size=15,color='white',transform=ax.transAxes,
                bbox=bbox_props) 
        ang = conv_az_to_polar(Dtm_az)
        if Dtm_az>180:
            ang -= 180
        ax.text(0.11, 0.90, " Pente ", ha="center", va="center",
                rotation=ang,
                size=15,color='white',transform=ax.transAxes)
        #slope value
        ax.text(0.24, 0.90, str(Dtm_sl)+' %', ha="center", va="center",
                size=15,color='#9a1010',transform=ax.transAxes)
    else:
        ax.text(0.15, 0.95,"Pente : "+ str(Dtm_sl)+' %', ha="center", va="center",
                size=15,color='#9a1010',transform=ax.transAxes)
    #North arrow
    bbox_props = dict(boxstyle="rarrow", fc=(0.7, 0.7, 0.7), ec="black", lw=2)
    ax.text(0.95, 0.92, "       ", ha="center", va="center",
            rotation=90,transform=ax.transAxes,
            size=15,
            bbox=bbox_props)
    ax.text(0.95, 0.92, " N ", ha="center", va="center",transform=ax.transAxes,           
            size=15)     
    plt.xlim(xmin, xmax)
    plt.ylim(ymax, ymin)
    ax.text(0.01, 0.01, str_Tarea, ha="left", va="bottom",            
            size=12,transform=ax.transAxes)    
    scalebar = ScaleBar(Csize,frameon=False,location="lower right")
    plt.gca().add_artist(scalebar)
    ax.set_axis_off()       
    plt.gca().set_aspect('equal', adjustable='box')
    plt.title(str_title, fontsize=20,fontweight='bold')
   
    if fig_name is not None:
        fig.savefig(fig_name, dpi=fig.dpi,bbox_inches='tight')
    #plt.show() 
    plt.close()

def get_Month_Name(Month):
    M_l = ["","Janvier","Février","Mars",
           "Avril","Mai","Juin","Juillet",
           "Août","Septembre","Octobre",
           "Novembre","Décembre"]
    return M_l[Month]


def build_plane_shape(Elevation,Csize,Dtm_sl,Dtm_az,
                      Stand_H,form,dim1,dim2,f_az,Temp_Rspace):
    
    f_dim1,f_dim2 = dim1*Stand_H,dim2*Stand_H        
    Dtm_cote = 5*max(f_dim1,f_dim2)/Csize
   
    Rast,nrows,ncols = build_inclined_plane(Csize,Dtm_cote,Dtm_sl,Elevation)
    
    #Apply Aspect to dtm
    Dtm = ndimage.rotate(Rast, -Dtm_az)
  
    if form=='rectangle':            
        Shape,vertices = draw_rectangle(f_dim1,f_dim2,f_az,Dtm,Csize,Temp_Rspace)               
            
    if form=='ellipse':    
        Shape,vertices = draw_ellipse(f_dim1,f_dim2,Dtm,f_az,Csize,Temp_Rspace)   
             
    cor = 0.5*Csize
    cory = 0
    if Dtm_az not in [0,90,180,270,390]:
        cory=-Csize   
    for i in range(vertices.shape[0]):       
        vertices[i,0]+=-cor
        vertices[i,1]+=cor+cory     
    
    hole_list = np.int32(np.argwhere(Shape==1))
    
    return Dtm,hole_list,Shape,vertices


def calc_surface_min(Sun_duration_min,Shape,nbdays,duree_max=1440):  
    Surface = np.zeros((duree_max,),dtype=np.uint16)
    Sd = np.int16(Sun_duration_min[Shape==1]/nbdays+0.5)
    val_list = np.unique(Sd)
    for val in val_list:
        Surface[val] = np.sum(Sd==val)
    return Surface

def calc_surface_plage(Surface,borne_sup=[1,2,4,6]):
    duree_max = Surface.shape[0]
    nbborne = len(borne_sup)
    tab_plage = np.zeros((nbborne+2,3),dtype=np.float32)
    tab_plage[0,2]=Surface[0]
    for i in range(1,nbborne+1):
        if i==1:
            tab_plage[i,0]=1
        else:
            tab_plage[i,0]=tab_plage[i-1,1]
        tab_plage[i,1]=borne_sup[i-1]*60
        idmin = int(tab_plage[i,0])
        idmax = int(tab_plage[i,1])        
        tab_plage[i,2]=np.sum(Surface[idmin:idmax])
    i+=1
    tab_plage[i,0]=tab_plage[i-1,1]
    tab_plage[i,1]=duree_max
    idmin = int(tab_plage[i,0])   
    tab_plage[i,2]=np.sum(Surface[idmin:])
    total = np.sum(tab_plage[:,2])
    tab_plage[:,2]=100*tab_plage[:,2]/total    
    tab_plage[:,0]/=60
    tab_plage[:,1]/=60
    return tab_plage
      


def Month_Av_Daily_sun_duration(Lat,Lon,Elevation,Tlaps,Csize,Dtm_sl,Dtm_az,
                                Stand_H,form,dim1,dim2,f_az,Rspace,Dtm,
                                hole_list,Shape,vertices,savefig=True,Month=6,borne_sup=[1,2,4,6]):
    
    Sun_posi,Az_val,nbdays = calc_sun_posi(Month,Tlaps,Lat,Lon,Elevation) 
    #Sun_rad,cloud_cover,Rmin = calc_radiation(Sun_posi,Dtm_az,Dtm_sl,Elevation,Month,Lat,Lon)
    f_dim1,f_dim2 = dim1*Stand_H,dim2*Stand_H   
                    
    Sun_duration_min = cd.sun_duration_virt_plane(hole_list, vertices,
                                                  Dtm,Sun_posi, Az_val,
                                                  f_dim1, f_dim2,Stand_H,Csize)  
    
    #Sun_duration_min,Sun_direct=cd.sun_dur_rad_virt_plane(hole_list, Mns,Shape,Sun_posi2,Az_val,Rmin)
    
    Sun_duration_hj2 = Sun_duration_min/(nbdays*60)
    Sun_duration_hj2[Shape==0]=-1
    
    Dtm_masked = np.ma.masked_array(np.copy(Dtm),Sun_duration_hj2!=-1)        
    fig_name = Rspace+"Duree_moyenne.png"
    
    #Legend title
    str_title = "Moyenne quotidienne en "+ str(get_Month_Name(Month))
      
    plot_Sun_Duration(Sun_duration_hj2,Csize,vertices,f_dim1,f_dim2,
                      Dtm_az,Dtm_sl,Stand_H,Dtm_masked,
                      str_title,form,fig_name,borne_sup)
    return Sun_duration_min,nbdays
    
def Month_Av_Daily_sun_duration2(Lat,Lon,Elevation,Tlaps,Csize,
                                 Stand_H,form,dim1,dim2,Dtm,
                                 hole_list,Shape,vertices,Month=6):
    
    Sun_posi,Az_val,nbdays = calc_sun_posi(Month,Tlaps,Lat,Lon,Elevation) 
    #Sun_rad,cloud_cover,Rmin = calc_radiation(Sun_posi,Dtm_az,Dtm_sl,Elevation,Month,Lat,Lon)
    f_dim1,f_dim2 = dim1*Stand_H,dim2*Stand_H   
                    
    Sun_duration_min = cd.sun_duration_virt_plane(hole_list, vertices,
                                                  Dtm,Sun_posi, Az_val,
                                                  f_dim1, f_dim2,Stand_H,Csize)  
    
    return Sun_duration_min,nbdays             

def Month_21_sun_duration(Lat,Lon,Elevation,Tlaps,Csize,Dtm_sl,Dtm_az,
                          Stand_H,form,dim1,dim2,f_az,Rspace,
                          Dtm,hole_list,Shape,vertices,borne_sup=[1,2,4,6]):
    
    f_dim1,f_dim2 = dim1*Stand_H,dim2*Stand_H    
    Dtm_masked = np.ma.masked_array(np.copy(Dtm),Shape==1)    
            
    Sun_posi,Az_val = get_sun_posi_21Month(Tlaps,Lat,Lon,Elevation)
    Sun_duration_min = cd.sun_duration_virt_plane21(hole_list, vertices,
                                                    Dtm,Sun_posi,Az_val,
                                                    f_dim1,f_dim2,Stand_H,
                                                    Csize)   
    fig_list = []
    for Month in range(1,13): 
        strMonth = str(Month) 
        if len(strMonth)<2:
            strMonth='0'+strMonth
        fig_name = Rspace+"Duree_21"+strMonth+".png" 
        fig_list.append(fig_name)
               
        Sun_duration_hj2 = Sun_duration_min[Month-1]/60
        Sun_duration_hj2[Shape==0]=-1
        
        #Legend title
        str_title = "21 "+str(get_Month_Name(Month))
          
        plot_Sun_Duration(Sun_duration_hj2,Csize,vertices,f_dim1,f_dim2,
                          Dtm_az,Dtm_sl,Stand_H,Dtm_masked,
                          str_title,form,fig_name,borne_sup)
       
    
def Day_sun_course(Lat,Lon,Elevation,Csize,Dtm_sl,Dtm_az,
                   Stand_H,form,dim1,dim2,f_az,Rspace,
                   Dtm,hole_list,Shape,vertices,
                   Tlaps=60,Month=6,Day=21):
    
    f_dim1,f_dim2 = dim1*Stand_H,dim2*Stand_H    
    Dtm_masked = np.ma.masked_array(np.copy(Dtm),Shape==1)    
    
    Sun_posi = calc_sun_posi_day(Lat,Lon,Elevation,Tlaps,Month,Day)    
    nbhours = Sun_posi.shape[0]
    
    Sun_shining = cd.sun_shining_virt_plane_day(hole_list,vertices,Dtm,
                                                Sun_posi,f_dim1, f_dim2,
                                                Stand_H,Csize)  
    
    #search hour to plot
    h_list = []
    for i in range(nbhours): 
        
        Raster = Sun_shining[i]
        if np.max(Raster)==0:
            continue
        else:
             h_list.append(i)
    hmin = np.min(h_list)
    h_list.insert(0,hmin-1)
    hmax = np.max(h_list)
    if hmax<nbhours-1:
        h_list.append(hmax+1)
    
    fig_list = []
    for i in h_list: 
        
        Raster = Sun_shining[i]      
       
        #Plot name
        str_hour = str(int(Sun_posi[i,2]))
        if len(str_hour)==1:
             str_hour ="0"+str_hour
        str_min = str(int(Sun_posi[i,3])) 
        if len(str_min)==1:
             str_min ="0"+str_min
        strfig = str_hour+"H"+str_min 
        fig_name = Rspace+"Ensoleillement_"+strfig+".png"
        fig_list.append(fig_name)         
        
        #Plot title        
        str_title = str(int(Sun_posi[i,2]))+"h"+str_min
            
        plot_SunDirect(Raster,Csize,vertices,f_dim1,f_dim2,Dtm_az,Dtm_sl,
                       Stand_H,Dtm_masked,str_title,form,fig_name)
               
    return fig_list

def seek_best_forms(DB_form,DB_Dtm_az,DB_Dtm_sl,DB_Stand_H,
                    DB_dim1,DB_dim2, 
                    DB_f_az,DB_Lat,DB_Lpente,DB_Surf,
                    s_Dtm_az,s_Dtm_sl,s_Stand_H,Lpente,
                    min_h,max_h,very_max_h,very_max_surf,
                    nb_config=4):
   
    if s_Dtm_az>200:
        Dtm_az = 400-s_Dtm_az
    else:
        Dtm_az = s_Dtm_az
    
    SlFor_OK,Dur_OK,Lpro_OK=True,True,True    
    Tab = np.zeros((nb_config,6))
    
    
    #### Select only config with s_Dtm_az,s_Dtm_sl,s_Stand_H  
    tp = ((DB_Dtm_az==Dtm_az)*(DB_Dtm_sl==s_Dtm_sl)*(DB_Stand_H==s_Stand_H))>0
    if np.sum(tp)==0:
        SlFor_OK,Dur_OK,Lpro_OK=False,False,False   
        return SlFor_OK,Dur_OK,Lpro_OK,Tab
    ####check if there is conf respecting max duration
    Surf_max_OK = np.sum(DB_Surf[:,very_max_h:],axis=1)/10.
    tp2 = (tp*(Surf_max_OK<=very_max_surf))>0
    if np.sum(tp2)==0:
        Dur_OK=False
        while np.sum(tp2)==0:
            very_max_surf+=1
            tp2 = (tp*(Surf_max_OK<=very_max_surf))>0 
    ####check if there is conf respecting Lprotect
    tp3 = (tp*(DB_Lpente<=Lpente))>0
    if np.sum(tp3)==0:
        Lpro_OK=False
        while np.sum(tp3)==0:
            Lpente+=1
            tp3 = (tp*(DB_Lpente<=Lpente))>0
        
    ####check if there is at list one config    
    tpfin = tp2*tp3
    nbconf = min(np.sum(tpfin),nb_config) 
    while nbconf==0:
        very_max_surf+=1
        Lpente+=1
        tpfin =(tp*(Surf_max_OK<=very_max_surf)*(DB_Lpente<=Lpente))>0 
        nbconf = min(np.sum(tpfin),nb_config) 
    
    ####Return best config or closest to user param 
    Surf_OK = np.int32(np.sum(DB_Surf[tpfin,min_h:max_h+1],axis=1))   
    cl = np.lexsort([Surf_max_OK[tpfin],DB_Lpente[tpfin],-Surf_OK])[0:nbconf]
    Tab = np.zeros((nbconf,6))
    for i in range(nbconf):
        Tab[i,0]=DB_form[tpfin][cl[i]]
        Tab[i,1]=DB_dim1[tpfin][cl[i]]/100
        Tab[i,2]=DB_dim2[tpfin][cl[i]]/100
        if s_Dtm_az>200:
            Tab[i,3]=-DB_f_az[tpfin][cl[i]]
        else:        
            Tab[i,3]=DB_f_az[tpfin][cl[i]]
        Tab[i,4]=min(Surf_OK[cl[i]]/10,100)
        Tab[i,5]=DB_Lpente[tpfin][cl[i]]
    return SlFor_OK,Dur_OK,Lpro_OK,Tab

def get_radiation(Tlaps,Lat,Lon,Elevation,Dtm_az,Dtm_sl):
    Rad_summer = 0
    Rad_an = 0
    for Month in range(1,13):
        Sun_posi,Az_val,nbdays = calc_sun_posi(Month,Tlaps,Lat,Lon,Elevation)
        Rad = calc_radiation(Sun_posi,Dtm_az,Dtm_sl,Elevation,Month,Lat,Lon)
        Rad_an+=Rad
        if Month>5 and Month<9:
            Rad_summer+=Rad
    return int(Rad_an+0.5),int(Rad_summer+0.5)


    
def plot_best_forms(Lat,Lon,Elevation,Tlaps,Csize,
                    s_Dtm_az,s_Dtm_sl,s_Stand_H,Tab_Best,Rspace,
                    hmin,hmax,hvmax,min_inc,max_inc,Temp_Rspace,
                    Month=6,borne_sup=[1,2,4,6]):
   
    nb_config = Tab_Best.shape[0]
    Dtm_az2 = s_Dtm_az*18/20
    
    Sun_posi,Az_val,nbdays = calc_sun_posi(Month,Tlaps,Lat,Lon,Elevation)   
    
    for i in range(nb_config): 
        form,dim1,dim2,f_az =  Tab_Best[i,:-2] 
        f_az2 = (f_az*18/20+Dtm_az2)%360
        if form==1 or form==3:
            form='rectangle'
        else:
            form='ellipse'
        Dtm,hole_list,Shape,vertices = build_plane_shape(Elevation,Csize,
                                                         s_Dtm_sl,Dtm_az2,
                                                         s_Stand_H,form,
                                                         dim1,dim2,f_az2,
                                                         Temp_Rspace) 
        f_dim1,f_dim2 = dim1*s_Stand_H,dim2*s_Stand_H   
                    
        Sun_duration_min = cd.sun_duration_virt_plane(hole_list, vertices,
                                                      Dtm,Sun_posi, Az_val,
                                                      f_dim1, f_dim2,
                                                      s_Stand_H,Csize)  
    
        Surf = calc_surface_min(Sun_duration_min,Shape,nbdays,duree_max=1440)
        surf_tot = np.sum(Surf)
        if min_inc:
            hmin2=hmin*60
        else:
            hmin2=hmin*60+1
        if max_inc:
            hmax2=hmax*60+1
        else:
            hmax2=hmax*60
            
        surf_OK = np.sum(Surf[hmin2:hmax2])
        Tab_Best[i,-2]=round(surf_OK/surf_tot*100,1)
        perc = str(Tab_Best[i,-2])+" %"
        
        Sun_duration_hj2 = Sun_duration_min/(nbdays*60)
        Sun_duration_hj2[Shape==0]=-1
    
        Dtm_masked = np.ma.masked_array(np.copy(Dtm),Sun_duration_hj2!=-1)        
        fig_name = Rspace+"Config_"+str(i+1)+".png"
    
        #Legend title
        str_title = "Config. "+ str(i+1)+" - Surface = "+perc
      
        plot_Sun_Duration(Sun_duration_hj2,Csize,vertices,f_dim1,f_dim2,
                          Dtm_az2,s_Dtm_sl,s_Stand_H,Dtm_masked,
                          str_title,form,fig_name,borne_sup)
    return Tab_Best

def conv_slid_to_DB_col(Plage_min,Plage_max,Durmax,min_inc,max_inc):
    min_list =  np.array([0,0     ,1,1     ,2,2     ,4,4     ,6,6],dtype=np.float32)
    min_list += np.array([0,0.0001,0,0.0001,0,0.0001,0,0.0001,0,0.0001])
    
    max_list =  np.array([0,1     ,1,2     ,2,4     ,4,6     ,6,24],dtype=np.float32)
    max_list -= np.array([0,0.0001,0,0.0001,0,0.0001,0,0.0001,0,0])
    
    Pla_list =  np.array([0,1,2,4,6,24])
    
    if min_inc:    
        min_h = np.argwhere(min_list==Pla_list[Plage_min])[0,0]
    else:
        min_h = np.argwhere(min_list>Pla_list[Plage_min])[0,0]
        
    if max_inc:
        max_h = np.argwhere(max_list==Pla_list[Plage_max])[0,0]
    else:
        max_h = np.max(np.argwhere(max_list<Pla_list[Plage_max]))
        
    Durmax_list = np.array([0,1,2,4,6])
    very_max_h = np.min(np.argwhere(min_list>Durmax_list[Durmax]))
    return min_h,max_h,very_max_h,Pla_list[Plage_min],Pla_list[Plage_max],Durmax_list[Durmax]
        
        
def process_best_form(DB_form,DB_Dtm_az,DB_Dtm_sl,DB_Stand_H,DB_dim1,DB_dim2, 
                      DB_f_az,DB_Lat,DB_Lpente,DB_Surf,
                      Lat,Lon,Elevation,Tlaps,Csize,Rspace,
                      s_Dtm_az,s_Dtm_sl,s_Stand_H,Lpente,
                      Plage_min,Plage_max,Durmax,min_inc,max_inc,very_max_surf,Temp_Rspace,
                      nb_config=4,Month=6,borne_sup=[1,2,4,6]):
    
    
    min_h,max_h,very_max_h,hmin,hmax,hvmax = conv_slid_to_DB_col(Plage_min,Plage_max,Durmax,min_inc,max_inc)
    
    SlFor_OK,Dur_OK,Lpro_OK,Tab_Best = seek_best_forms(DB_form,DB_Dtm_az,DB_Dtm_sl,DB_Stand_H,
                                                       DB_dim1,DB_dim2, 
                                                       DB_f_az,DB_Lat,DB_Lpente,DB_Surf,
                                                       s_Dtm_az,s_Dtm_sl,s_Stand_H,Lpente,
                                                       min_h,max_h,very_max_h,very_max_surf,
                                                       nb_config)
    if not SlFor_OK:
        mess= "Aucune configuration ne correspond au trio "
        mess+= "(Exposition, Pente, Hauteur de peuplement) renseigné\n"
    else:
        nbconf = str(Tab_Best.shape[0])
        mess = nbconf +" configuration(s) trouvée(s).\n\n"
    if not Dur_OK:
        mess+= "Attention : aucune configuration ne respecte la condition : "
        mess+= "Proportion de la surface avec Durée < Durée max\n\n"
        mess+= "Les configurations proposées minimisent cette surface.\n\n"
    if not Lpro_OK:
        mess+= "Attention : aucune configuration ne respecte la condition de longueur "
        mess+= "maximale le long de la pente\n\n"
        mess+= "Les configurations proposées minimisent cette longueur."
        
    
    if Tab_Best[0,0]>0:
        Tab_Best = plot_best_forms(Lat,Lon,Elevation,Tlaps,Csize,
                                   s_Dtm_az,s_Dtm_sl,s_Stand_H,Tab_Best,Rspace,
                                   hmin,hmax,hvmax,min_inc,max_inc,Temp_Rspace,
                                   Month,borne_sup)
        
    Tabparam = np.zeros((13,),dtype=np.float32)
    Tabparam[0:6] = Lat,Lon,Elevation,s_Dtm_az,s_Dtm_sl,s_Stand_H
    Tabparam[6:] = hmin+0.1*(1-min_inc),hmax-0.1*(1-max_inc),hvmax,very_max_surf,Lpente,Dur_OK,Lpro_OK
    
    np.savez(Rspace+"Params.npz",Param=Tabparam,Tab_Best=Tab_Best)
    
    
    return Tab_Best,mess

    
    
    
def get_rad(Radiations,Lat,Lon,Elevation,Dtm_az,Dtm_sl,Clouds,tree):
    #### Get cloud cover of local place
    Dists,Inds = tree.query([Lat,Lon], k=4) 
    if np.min(Dists)<1:
        if Dists[0]==0:
            cloud_cover= Clouds[Inds[0],2:]
        else: 
            cloud_cover= np.zeros((12,))     
            w = 0
            for i,ind in enumerate(Inds):            
                if Dists[i]>1.0:
                    break
                cloud_cover+=1/Dists[i]*Clouds[ind,2:]
                w+=1/Dists[i]
            
            cloud_cover /=w
            Kc = (1-0.75*(cloud_cover/100.)**3.4)
            
    else:
        Kc= np.ones((12,))      
    
    #### Select only position in Lat Elev range
    tp = ((np.abs(Radiations[:,0]/100.-Lat)<0.5)*(np.abs(Radiations[:,3]-Elevation)<300))>0
    
    if np.sum(tp)>0:  
        Lat_list = np.unique(Radiations[tp,0])
        Elev_list = np.unique(Radiations[tp,3])
        nbligne = int(np.sum(tp)/(Lat_list.shape[0]*Elev_list.shape[0])) 
        
        if Lat_list.shape[0]==1 and  Elev_list.shape[0]==1 :
            #cas n°1 : Lat et Elev sont dans la bdd
            Rad = Radiations[tp]
        elif Lat_list.shape[0]==1 and  Elev_list.shape[0]==2:
            #cas n°2 : Lat dans la bdd / Elev non
            Rad = np.zeros((nbligne,17))
            tp0 = (tp*(Radiations[:,3]==Elev_list[0]))>0
            tp1 = (tp*(Radiations[:,3]==Elev_list[1]))>0
            Dist = np.abs(Elevation-Elev_list)/300
            Rad[:,0]=Lat
            Rad[:,3]=Elevation
            Rad[:,1:3]=Radiations[tp1,1:3]
            for i in range(4,17):
                Rad[:,i]=(1/Dist[0]*Radiations[tp0,i]+1/Dist[1]*Radiations[tp1,i])/np.sum(1/Dist)
        elif Lat_list.shape[0]==2 and  Elev_list.shape[0]==1:
            #cas n°3 : Elev dans la bdd / Lat  non
            Rad = np.zeros((nbligne,17))
            tp0 = (tp*(Radiations[:,0]==Lat_list[0]))>0
            tp1 = (tp*(Radiations[:,0]==Lat_list[1]))>0
            Dist = np.abs(Lat*100-Lat_list)/50
            Rad[:,0]=Lat
            Rad[:,3]=Elevation
            Rad[:,1:3]=Radiations[tp1,1:3]
            for i in range(4,17):
                Rad[:,i]=(1/Dist[0]*Radiations[tp0,i]+1/Dist[1]*Radiations[tp1,i])/np.sum(1/Dist)        
        else:
            #cas n°4 : Elev & Lat hors BDD
            Rad = np.zeros((nbligne,17))
            tp0 = (tp*(Radiations[:,0]==Lat_list[0])*(Radiations[:,3]==Elev_list[0]))>0
            tp1 = (tp*(Radiations[:,0]==Lat_list[1])*(Radiations[:,3]==Elev_list[0]))>0
            tp2 = (tp*(Radiations[:,0]==Lat_list[0])*(Radiations[:,3]==Elev_list[1]))>0
            tp3 = (tp*(Radiations[:,0]==Lat_list[1])*(Radiations[:,3]==Elev_list[1]))>0
            DistLat = np.abs(Lat*100-Lat_list)/50
            DistElev = np.abs(Elevation-Elev_list)/300
            Dist = np.array([DistLat[0]*DistElev[0],
                             DistLat[1]*DistElev[0],
                             DistLat[0]*DistElev[1],
                             DistLat[1]*DistElev[1]])
            Rad[:,0]=Lat
            Rad[:,3]=Elevation
            Rad[:,1:3]=Radiations[tp1,1:3]
            for i in range(4,17):            
                Rad[:,i] = 1/Dist[0]*Radiations[tp0,i]
                Rad[:,i]+= 1/Dist[1]*Radiations[tp1,i]
                Rad[:,i]+= 1/Dist[2]*Radiations[tp2,i]
                Rad[:,i]+= 1/Dist[3]*Radiations[tp3,i]
                Rad[:,i]/= np.sum(1/Dist)
            
        R_an = np.sum(Rad[:,4:],axis=1)
        R_an_c = np.sum(Rad[:,4:16]*Kc,axis=1)+Rad[:,16]
        
        Res = np.zeros((4,4),dtype=np.float32)  
        ind = np.argmin(R_an_c)     
        Res[0,:]=Rad[ind,1],Rad[ind,2],R_an[ind],R_an_c[ind]
        ind = np.argmax(R_an_c)    
        Res[1,:]=Rad[ind,1],Rad[ind,2],R_an[ind],R_an_c[ind]
        Res[2,:]=Rad[0,1],Rad[0,2],R_an[0],R_an_c[0]
        
          
        if Dtm_az>200:
            az=400-Dtm_az
        else:
            az=Dtm_az
        
        inds = np.argwhere(((np.abs(Rad[:,1]-az)<25)*(np.abs(Rad[:,2]-Dtm_sl)<10))>0)[:,0]
        if inds.shape[0]==1:
            Ran_conf = R_an[inds[0]]
            Ranc_conf = R_an_c[inds[0]]
        else:  
            Ran_conf = 0
            Ranc_conf = 0
            Dist = []
            for ind in inds:            
                d_sl = abs(Rad[ind,2]-Dtm_sl)/10.
                d_az = abs(Rad[ind,1]-Dtm_az)/25.
                d = d_sl*d_az
                if ind==0:
                    d = d_sl
                if min(d_sl,d_az)==0:
                    d=max(d_sl,d_az)
                Dist.append(d)
                Ran_conf+=1/d*R_an[ind]
                Ranc_conf+=1/d*R_an_c[ind]
            Dist = np.sum(1/np.array(Dist))
            Ran_conf/=Dist
            Ranc_conf/=Dist      
        Res[3,:]=Dtm_az,Dtm_sl,Ran_conf,Ranc_conf
        
    else:
        #Calculation of radiation range on lat Lon position
        Sun_posi = get_solar_ephem(1,Lat,Lon,Elevation)
        Tab = np.zeros((108,17),dtype=np.int16) 
        line=0
        for sl in range(0,120,10):
            for az in range(0,225,25):
                if sl==0 and az>0:
                    break
                Tab[line,0]=int(Lat*100)
                Tab[line,1]=az
                Tab[line,2]=sl
                Tab[line,3]=Elevation
                Tab[line,4:]=calc_radiation_year(Sun_posi,Elevation,
                                                 az*18/20,sl)
                line+=1 
                
        Tab=Tab[:line]
        R_an = np.sum(Tab[:,4:],axis=1)
        R_an_c = np.sum(Tab[:,4:16]*Kc,axis=1)+Tab[:,16]  
        Res = np.zeros((4,4),dtype=np.float32)  
        ind = np.argmin(R_an_c)     
        Res[0,:]=Tab[ind,1],Tab[ind,2],R_an[ind],R_an_c[ind]
        ind = np.argmax(R_an_c)    
        Res[1,:]=Tab[ind,1],Tab[ind,2],R_an[ind],R_an_c[ind]
        Res[2,:]=Tab[0,1],Tab[0,2],R_an[0],R_an_c[0]
        
        inds = np.argwhere(((np.abs(Tab[:,1]-az)<25)*(np.abs(Tab[:,2]-Dtm_sl)<10))>0)[:,0]
        if inds.shape[0]==1:
            Ran_conf = R_an[inds[0]]
            Ranc_conf = R_an_c[inds[0]]
        else:  
            Ran_conf = 0
            Ranc_conf = 0
            Dist = []
            for ind in inds:            
                d_sl = abs(Tab[ind,2]-Dtm_sl)/10.
                d_az = abs(Tab[ind,1]-Dtm_az)/25.
                d = d_sl*d_az
                if ind==0:
                    d = d_sl
                if min(d_sl,d_az)==0:
                    d=max(d_sl,d_az)
                Dist.append(d)
                Ran_conf+=1/d*R_an[ind]
                Ranc_conf+=1/d*R_an_c[ind]
            Dist = np.sum(1/np.array(Dist))
            Ran_conf/=Dist
            Ranc_conf/=Dist      
        Res[3,:]=Dtm_az,Dtm_sl,Ran_conf,Ranc_conf   
    return Res       
    

def plot_rad_scale_v(Radiations,Lat,Lon,Elevation,Dtm_az,Dtm_sl,
                     Clouds,tree,fig_name=None,col=3):  
    
    Res = get_rad(Radiations,Lat,Lon,Elevation,Dtm_az,Dtm_sl,Clouds,tree)  
    plt.ioff()
    fig, ax = plt.subplots(figsize=(1,4),dpi=100) 
    cmap = mpl.colors.ListedColormap(["#0066FF", "#00CCFF",
                                     "#FFCC00", "#FF6600","#FF0000"])
    bmin = Res[0,col]
    bmax = Res[1,col]
    ran = bmax-bmin
    
    bef = (Res[2,col]-bmin)/2.5
    aft = (bmax-Res[2,col])/2.5
    pla = Res[2,col]
    config =  Res[3,col]
    bounds = [bmin,
              bmin+bef,
              bmin+2*bef,
              bmax-2*aft,
              bmax-aft,
              bmax]
    tick_posi = [bmin,                
                 pla,                 
                 bmax]    
    
    dlabel = [str(int(bmin+0.5)),              
              "Plat : "+str(int(pla+0.5)),             
              str(int(bmax+0.5))]
    
    norm = mpl.colors.BoundaryNorm(bounds, cmap.N)
    cb = mpl.colorbar.ColorbarBase(ax, cmap=cmap,
                                   norm=norm,                                  
                                   extend='both',
                                   # Make the length of each extension
                                   # the same as the length of the
                                   # interior colors:
                                   extendfrac=0.05,                                   
                                   ticks=tick_posi,
                                   spacing='proportional',                               
                                   orientation='vertical',
                                   drawedges =False)
        
    cb.ax.plot([0,25*bmax],[config,config],"#000000",linewidth=3)
    cb.ax.plot([0,25*bmax],[pla,pla],"#FFFFFF",linewidth=2)
    ax1 = cb.ax
    ax1.set_yticklabels(dlabel,size=12)  
    
    bbox_props = dict(boxstyle="rarrow,pad=0.0", fc="#636363", ec="#000000", lw=1)
    cb.ax.text(bmin-0.6*ran, config, "    ", ha="left", va="center", rotation=0, size=16, bbox=bbox_props)
    lab = str(int(Res[3,col]+0.5))
    cb.ax.text(bmin-0.85*ran, config, lab, ha="center", va="center", rotation=90, size=11)
    
    
    labmin='('+str(int(Res[0,0]+0.5))+ " gr - "+str(int(Res[0,1]+0.5))+" %)"
    cb.ax.text(1.03*Res[1,col], bmin-0.06*ran, labmin, ha="left", va="center", rotation=00, size=11)
    labmax='('+str(int(Res[1,0]+0.5))+ " gr - "+str(int(Res[1,1]+0.5))+" %)"
    cb.ax.text(1.03*Res[1,col], bmax-0.06*ran, labmax, ha="left", va="center", rotation=00, size=11)
      
    titre = 'Rayonnement solaire (MJ/an/m²)'
    plt.title(titre,fontweight="bold")
   
    if fig_name is not None:
        fig.savefig(fig_name, dpi=fig.dpi,bbox_inches='tight')
    #plt.show() 
    plt.close()

def save_xls(filename,Rspace_O):    
    Data = np.load(Rspace_O+"Params.npz")
    Tab_Best = Data["Tab_Best"]
    Params = Data["Param"]
    Lat,Lon,Elevation,Dtm_az,Dtm_sl,Stand_H,hmin,hmax=Params[0:8]
    hvmax,SurfDmax,LProtec,Dur_OK,Lpro_OK=np.int32(Params[8:])
            
    nb_config = Tab_Best.shape[0]
    mess=""
    if not Dur_OK and Lpro_OK:
        mess+= "Attention : aucune configuration ne respecte la condition : "
        mess+= "Proportion de la surface avec Durée < Durée max. "
        mess+= "Les configurations proposées minimisent cette surface."
        
    if Dur_OK and not Lpro_OK:    
        mess+= "Attention : aucune configuration ne respecte la condition de longueur "
        mess+= "maximale le long de la pente. "
        mess+= "Les configurations proposées minimisent cette longueur."
    
    if not Dur_OK and not Lpro_OK:    
        mess+= "Attention : aucune configuration ne respecte les conditions "
        mess+= "(1) de longueur maximale le long de la pente et "
        mess+= "(2) de proportion de la surface avec Durée < Durée max\n" 
        mess+= "Les configurations proposées minimisent ces paramètres."
    
    wb = load_workbook("./ressource/LUMIFOR_Template.xlsx")
    ws = wb.active
    
    #Save entete
    my_png = openpyxl.drawing.image.Image("./ressource/Logo_Sylvalab.png")
    ws.add_image(my_png, 'A1')
    my_png = openpyxl.drawing.image.Image("./ressource/Logo-ONF.png")
    ws.add_image(my_png, 'H1')
        
    #Save radiation
    my_png = openpyxl.drawing.image.Image(Rspace_O+"Rayonnement.png")
    ws.add_image(my_png, 'E8')
    
    
    #Save config
    cells = ["","A60","E60","A79","E79"]
    for i in range(1,nb_config+1):
        my_png = openpyxl.drawing.image.Image(Rspace_O+"Config_"+str(i)+".png")
        my_png.width=324
        my_png.height=384
        ws.add_image(my_png, cells[i])
    
    #Save Param
    ws.cell(row=8,column=2,value=Lat) 
    ws.cell(row=9,column=2,value=Lon) 
    
    ws.cell(row=10,column=2,value=Elevation) 
    ws.cell(row=12,column=2,value=Dtm_az) 
    ws.cell(row=13,column=2,value=Dtm_sl) 
    
    ws.cell(row=19,column=2,value=Stand_H)
    
    ws.cell(row=32,column=3,value=round(hmin,0)) 
    if hmin==int(hmin):
        ws.cell(row=32,column=5,value='(inclus)') 
    else:
        ws.cell(row=32,column=5,value='(exclus)')         
    ws.cell(row=33,column=3,value=round(hmax,0))
    if hmax==int(hmax):
        ws.cell(row=33,column=5,value='(inclus)') 
    else:
        ws.cell(row=33,column=5,value='(exclus)')       
    ws.cell(row=35,column=3,value=round(hvmax,0))
    ws.cell(row=35,column=6,value=SurfDmax)
    
    if Dtm_sl>36 and LProtec<(100*Stand_H):
        ws.cell(row=39,column=4,value=LProtec)
    else:
        ws.cell(row=39,column=4,value="NA")
    
    #Fill Table
    for i in range(nb_config):
        #Surface
        ws.cell(row=53+i,column=2,value=Tab_Best[i,4])       
        #Forme
        form = Tab_Best[i,0]
        if form==1:
            F="Rectangle"
        elif form==2:
            F="Ellipse"
        elif form==3:
            F="Carré"
        else:
            F="Rond"
        ws.cell(row=53+i,column=3,value=F) 
        #Dimension
        D = str(Tab_Best[i,1])+" x "+str(Tab_Best[i,2])
        ws.cell(row=53+i,column=4,value=D) 
        #Orientation
        O = int(Tab_Best[i,3])
        ws.cell(row=53+i,column=5,value=O) 
        #Surfacetotale
        f_dim1 = Tab_Best[i,1]*Stand_H
        f_dim2 = Tab_Best[i,2]*Stand_H
        if form==1 or form==3:
            surf = f_dim1*f_dim2   
        else:
            surf = 0.5*f_dim1*0.5*f_dim2*math.pi
        ws.cell(row=53+i,column=6,value=surf) 
        #Lepnte
        ws.cell(row=53+i,column=7,value=Tab_Best[i,5]) 
    
    ws.cell(row=50,column=1).alignment = Alignment(wrapText=True,vertical="top",horizontal='left')
    ws.cell(row=50,column=1,value=mess)    
    ws.protection.sheet = True
    
    wb.save(filename)

def Distplan(y, x,yE, xE):
    return math.sqrt((y-yE)*(y-yE)+(x-xE)*(x-xE))


def draw_rectangle2(r_dim1,r_dim2,rAz):
    rLo,rLa=max(r_dim1,r_dim2),min(r_dim1,r_dim2)
    Dtm_cote = 5*max(r_dim1,r_dim2)
    ymid,xmid = int(Dtm_cote/2),int(Dtm_cote/2)   
    
    rect = Rectangle((ymid,xmid), rLa, rLo, -rAz) 
    vertices = rect.get_verts()     # get the vertices from the ellipse object
    
    xmid2 = 0.5*(np.min(vertices[:,0])+np.max(vertices[:,0]))
    ymid2 = 0.5*(np.min(vertices[:,1])+np.max(vertices[:,1]))
    
    corx = xmid-xmid2   
    cory = ymid-ymid2 
        
    vertices[:,0]+=corx
    vertices[:,1]+=cory
    
    rectangle = LineString(vertices)
    A = Point(xmid,0)
    B = Point(xmid,Dtm_cote)
    AB = LineString([(A.x,A.y), (B.x,B.y)])
    
    intersection = rectangle.intersection(AB)
    I1 = intersection[0]
    I2 = intersection[1]
        
    return Distplan(I1.y,I1.x,I2.y,I2.x)

def draw_ellipse2(e_dim1,e_dim2,rAz):   
    
    rLo,rLa=max(e_dim1,e_dim2),min(e_dim1,e_dim2)
    Dtm_cote = 5*max(e_dim1,e_dim2)
    ymid,xmid = int(Dtm_cote/2),int(Dtm_cote/2)

    ellipse = Ellipse((xmid, ymid), rLa, rLo, -rAz) 
    vertices = ellipse.get_verts()     # get the vertices from the ellipse object    
    rectangle = LineString(vertices)
    A = Point(xmid,0)
    B = Point(xmid,Dtm_cote)
    AB = LineString([(A.x,A.y), (B.x,B.y)])
    
    intersection = rectangle.intersection(AB)
    I1 = intersection[0]
    I2 = intersection[1]  
    
    return int(Distplan(I1.y,I1.x,I2.y,I2.x)+0.5)

def get_Lpente(Stand_H,form,dim1,dim2,f_az,Dtm_sl):    
    f_dim1,f_dim2 = dim1*Stand_H,dim2*Stand_H  
    f_az2=f_az*18/20
    if form=='rectangle':            
        Dist = draw_rectangle2(f_dim1,f_dim2,f_az2)               
            
    if form=='ellipse':    
        Dist = draw_ellipse2(f_dim1,f_dim2,f_az2)               
    return Dist*math.sqrt(100*100+Dtm_sl*Dtm_sl)/100


def transform_coordinate(x,y,epsg1="epsg:2154",epsg2="epsg:4326"):
    transformer = Transformer.from_crs(epsg1, epsg2)
    return transformer.transform(x, y)

def load_float_raster(raster_file):
    dataset = gdal.Open(raster_file,gdal.GA_ReadOnly)
    cols = dataset.RasterXSize
    rows = dataset.RasterYSize    
    geotransform = dataset.GetGeoTransform()
    xmin = geotransform[0]
    xmax = xmin + geotransform[1]*cols
    ymax = geotransform[3]
    ymin = geotransform[3] + geotransform[5]*rows
    Extent = [xmin,xmax,ymin,ymax]
    Csize = abs(geotransform[1])
    proj = dataset.GetProjection()
    dataset_val = dataset.GetRasterBand(1)
    nodatavalue = dataset_val.GetNoDataValue()      
    Array = dataset_val.ReadAsArray()
    if nodatavalue is not None:
        Array[Array==nodatavalue]=-9999
    Array[np.isnan(Array)]=-9999
    dataset.FlushCache()
   
    return np.float32(Array),Extent,Csize,proj

def shapefile_to_np_array(file_name,Extent,Csize,attribute_name,order_field=None,order=None):
    """
    Convert shapefile to numpy array
    ----------
    Parameters
    ----------
    file_name:              string      Complete name of the shapefile to convert
    Extent:                 list        Extent of the array : [xmin,xmax,ymin,ymax]
    Csize:                  int, float  Cell resolution of the output array
    attribute_name:         string      Attribute name of the field used for rasterize
    order_field (optional): string      Attribute name of the field used to order the rasterization
    order (optional):       string      Sorting type : 'ASC' for ascending or 'DESC' descending

    Returns
    -------
    mask_array :            ndarray int32
    ----------
    Examples
    --------
    >>> import ogr,gdal
    >>> import numpy as np
    >>> mask_array = shapefile_to_np_array("Route.shp",[0,1000,0,2000],5,"Importance","Importance",'ASC')
    """
    #Recupere les dimensions du raster ascii
    xmin,xmax,ymin,ymax = Extent[0],Extent[1],Extent[2],Extent[3]
    nrows,ncols = int((ymax-ymin)/float(Csize)+0.5),int((xmax-xmin)/float(Csize)+0.5)
    # Get information from source shapefile
    orig_data_source = ogr.Open(file_name)
    source_ds = ogr.GetDriverByName("Memory").CopyDataSource(orig_data_source, "")
    source_layer = source_ds.GetLayer()
    if order:
        source_layer_ordered = source_ds.ExecuteSQL('SELECT * FROM '+str(source_layer.GetName())+' ORDER BY '+order_field+' '+order)
    else:source_layer_ordered=source_layer
    source_srs = source_layer.GetSpatialRef()
    # Initialize the new memory raster
    maskvalue = 1    
    xres=float(Csize)
    yres=float(Csize)
    geotransform=(xmin,xres,0,ymax,0, -yres)    
    target_ds = gdal.GetDriverByName('MEM').Create('', int(ncols), int(nrows), 1, gdal.GDT_Int32)
    target_ds.SetGeoTransform(geotransform)
    if source_srs:
        # Make the target raster have the same projection as the source
        target_ds.SetProjection(source_srs.ExportToWkt())
    else:
        # Source has no projection (needs GDAL >= 1.7.0 to work)
        target_ds.SetProjection('LOCAL_CS["arbitrary"]')
    # Rasterize
    err = gdal.RasterizeLayer(target_ds, [maskvalue], source_layer_ordered,options=["ATTRIBUTE="+attribute_name,"ALL_TOUCHED=TRUE"])
    if err != 0:
        raise Exception("error rasterizing layer: %s" % err)
    else:
        target_ds.FlushCache()
        mask_arr = target_ds.GetRasterBand(1).ReadAsArray()
        return mask_arr
 
def ligh_line(mat,Dmin):
    mat[0,3]=1
    for i in range(1,mat.shape[0]):
        if (mat[i,2]-mat[i-1,2])>Dmin:
            mat[i,3]=1
    return mat[mat[:,3]>0]    
 
def create_buffer(Csize,Lmax):
    Lcote = Lmax+1.5*Csize
    xmin,xmax,ymin,ymax = -Lcote,Lcote,-Lcote,Lcote
    Buffer_cote = int((Lmax)/Csize+1.5)
    Dir_list = range(0,91,1)
    Row_line = np.zeros((len(Dir_list),3*Buffer_cote),dtype=np.int16)
    Col_line = np.zeros((len(Dir_list),3*Buffer_cote),dtype=np.int16)    
    Nbpix_line = np.zeros((len(Dir_list),),dtype=np.int16)    
    Dmin = sqrt(2)*Csize*0.5
    for az in Dir_list:
        #Fill line info
        X1,Y1,mask_arr=from_az_to_arr(xmin,xmax,ymin,ymax,Csize,Lmax,az)        
        inds=np.argwhere(mask_arr==1)-Buffer_cote
        mat = np.zeros((inds.shape[0],4))
        mat[:,:2] = inds
        mat[:,2] = Csize*np.sqrt(mat[:,0]**2+mat[:,1]**2)
        ind = np.lexsort((mat[:,1],mat[:,2]))
        mat = mat[ind]
        mat = ligh_line(mat,Dmin)
        nb_pix=mat.shape[0]
        Row_line[az,0:nb_pix]=mat[:,0]
        Col_line[az,0:nb_pix]=mat[:,1]       
        Nbpix_line[az] = nb_pix        
    NbpixmaxL = np.max(Nbpix_line)
    return Row_line[:,0:NbpixmaxL],Col_line[:,0:NbpixmaxL],Nbpix_line

def from_az_to_arr(xmin,xmax,ymin,ymax,Csize,Lmax,az):    
    X1 = sin(radians(az))*Lmax
    Y1 = cos(radians(az))*Lmax
    #Initialize raster info
    nrows,ncols = int((ymax-ymin)/float(Csize)+0.5),int((xmax-xmin)/float(Csize)+0.5)    
    #create polygon object:
    driver = ogr.GetDriverByName('Memory')
    datasource = driver.CreateDataSource('')
    source_srs=osr.SpatialReference()
    source_srs.ImportFromEPSG(2154)
    layer = datasource.CreateLayer('layerName',source_srs,geom_type=ogr.wkbLineString)
    layerDefinition = layer.GetLayerDefn()
    new_field = ogr.FieldDefn('ID', ogr.OFTInteger)
    layer.CreateField(new_field)
    line = ogr.Geometry(ogr.wkbLineString)
    line.AddPoint(0,0)
    line.AddPoint(X1,Y1)
    feature = ogr.Feature(layerDefinition)
    feature.SetGeometry(line)
    feature.SetFID(az)
    feature.SetField('ID',1)
    layer.CreateFeature(feature)    
    feature.Destroy()     
    # Initialize the new memory raster      
    maskvalue = 1    
    xres=float(Csize)
    yres=float(Csize)
    geotransform=(xmin,xres,0,ymax,0, -yres)    
    target_ds = gdal.GetDriverByName('MEM').Create('', int(ncols), int(nrows), 1, gdal.GDT_Int16)
    target_ds.SetGeoTransform(geotransform)
    if source_srs:
        # Make the target raster have the same projection as the source
        target_ds.SetProjection(source_srs.ExportToWkt())
    else:
        # Source has no projection (needs GDAL >= 1.7.0 to work)
        target_ds.SetProjection('LOCAL_CS["arbitrary"]')
    # Rasterize
    err = gdal.RasterizeLayer(target_ds, [maskvalue], layer,options=["ATTRIBUTE=ID","ALL_TOUCHED=TRUE"])
    if err != 0:
        raise Exception("error rasterizing layer: %s" % err)
    else:
        target_ds.FlushCache()
        mask_arr = target_ds.GetRasterBand(1).ReadAsArray()
    datasource.Destroy()
    return X1,Y1,mask_arr


###############################################################################
### Function terrain
###############################################################################

def load_Azimuts(strCsize,Csize):
    Tab = np.load('ressource/Azimut.npz')   
    Rows=np.int16(Tab["Row_"+strCsize])
    Cols=np.int16(Tab["Col_"+strCsize])
    Nbpix=np.int16(Tab["Nbpix_"+strCsize])
    MNT_100 = np.int16(Tab["MNT100"])
    Ext_100 = np.float32(Tab["Extent100"]) 
    
    Dist = Csize*np.sqrt(np.power(np.float32(Rows),2)+np.power(np.float32(Cols),2))
    
    return Rows,Cols,Dist,Nbpix,MNT_100,Ext_100

def raster_get_info(in_file_name):
    source_ds = gdal.Open(in_file_name)    
    src_proj = osr.SpatialReference(wkt=source_ds.GetProjection())
    src_ncols = source_ds.RasterXSize
    src_nrows = source_ds.RasterYSize
    xmin,Csize_x,a,ymax,b,Csize_y = source_ds.GetGeoTransform()
    ymin = ymax+src_nrows*Csize_y
    nodata = source_ds.GetRasterBand(1).GetNoDataValue()
    names = ['ncols', 'nrows', 'xllcorner', 'yllcorner', 'cellsize','NODATA_value']
    values = [src_ncols,src_nrows,xmin,ymin,Csize_x,nodata]
    Extent = [xmin,xmin+src_ncols*Csize_x,ymin,ymax]
    return Csize_x

def ArrayToGtiff(Array,file_name,Extent,nrows,ncols,Csize,road_network_proj,nodata_value,raster_type='INT32'):
    """
    Create Tiff raster from numpy array   
    ----------
    Parameters
    ----------
    Array:             np.array    Array name
    file_name:         string      Complete name of the output raster
    Extent:            list        Extent of the area : [xmin,xmax,ymin,ymax]
    nrows:             int         Number of rows in the array
    ncols:             int         Number of columns in the array
    Csize:             int, float  Cell resolution of the array  
    road_network_proj: string      Spatial projection
    nodata_value:      int, float  Value representing nodata in the array
    raster_type:       string      'INT32' (default),'UINT8','UINT16','FLOAT32','FLOAT16'

    """
    xmin,xmax,ymin,ymax=Extent[0],Extent[1],Extent[2],Extent[3]
    xres=(xmax-xmin)/float(ncols)
    yres=(ymax-ymin)/float(nrows)
    geotransform=(xmin,xres,0,ymax,0, -yres)
    if raster_type=='INT32':
        #-2147483648 to 2147483647
        DataType = gdal.GDT_Int32    
    elif raster_type=='UINT8':
        #0 to 255
        DataType = gdal.GDT_Byte
    elif raster_type=='UINT16':
        #0 to 65535    
        DataType = gdal.GDT_UInt16
    elif raster_type=='INT16':
        #-32768 to 32767 
        DataType = gdal.GDT_Int16
    elif raster_type=='FLOAT32':
        #Single precision float: sign bit, 8 bits exponent, 23 bits mantissa
        DataType = gdal.GDT_Float32
    elif raster_type=='FLOAT16':
        #Half precision float: sign bit, 5 bits exponent, 10 bits mantissa
        DataType = gdal.GDT_Float16
    target_ds = gdal.GetDriverByName('GTiff').Create(file_name+'.tif', int(ncols), int(nrows), 1, DataType)
    target_ds.SetGeoTransform(geotransform)
    target_ds.SetProjection(road_network_proj)
    target_ds.GetRasterBand(1).WriteArray( Array )
    target_ds.GetRasterBand(1).SetNoDataValue(nodata_value)
    target_ds.GetRasterBand(1).GetStatistics(0,1)
    target_ds.FlushCache()

def get_proj_from_road_network(road_network_file):
    source_ds = ogr.Open(road_network_file)
    source_layer = source_ds.GetLayer()    
    source_srs = source_layer.GetSpatialRef()
    return source_srs.ExportToWkt(),source_srs

def get_centroids(Trouee_file):
    # Get information from source shapefile
    ds = ogr.Open(Trouee_file)    
    layer = ds.GetLayer()   
    srs = layer.GetSpatialRef()
    srs.AutoIdentifyEPSG()
    if srs.GetAuthorityCode(None) is None:
        if srs.GetName()=='RGF93_Lambert_93':
            srs.SetFromUserInput("EPSG:2154")
            srs.AutoIdentifyEPSG()
            epsg = "epsg:"+srs.GetAuthorityCode(None)    
        else:
            return False
    else:
        epsg = "epsg:"+srs.GetAuthorityCode(None)
    nb_trouee = layer.GetFeatureCount()
    Cent = np.zeros((nb_trouee,6),dtype=np.float32)
    for i,tr in enumerate(layer):
        geom = tr.geometry()       
        Cent[i,0]=tr.GetField("ID_OUV")
        Cent[i,1]=tr.GetField("EXIST")
        Cent[i,2:4]=geom.Centroid().GetX(),geom.Centroid().GetY()
        Cent[i,4:]=transform_coordinate(Cent[i,2],Cent[i,3],
                                        epsg1=epsg,epsg2="epsg:4326")
    return Cent

def get_slope_az(Cent,Ext_100,MNT_100,Csize=100): 
    Rows,Cols,Nbpix=create_buffer(Csize,25000)
    Dist = Csize*np.sqrt(np.power(np.float32(Rows),2)+np.power(np.float32(Cols),2))
    
    x0,xmax,y0,ymax=Ext_100
    nrows,ncols = MNT_100.shape

    slAz = np.zeros((Cent.shape[0],360),dtype=np.float32)
    
    for i,pt in enumerate(Cent):
        x,y=pt[2],pt[3]

        yr = int((ymax-y)/Csize)
        xr = int((x-x0)/Csize)
       
        for az in range(360):
            if az<=90:
                az2=az
                signx=1
                signy=1
            elif az>90 and az<=180:
                az2=180-az       
                signx=1
                signy=-1
            elif az>180 and az<270:
                az2=az-180
                signx=-1
                signy=-1        
            else:
                az2=360-az
                signx=-1
                signy=1   
            nbpix = Nbpix[az2]
            xcol = xr+signx*Cols[az2,1:nbpix]
            #check if xcol>=0 and xcol<ncol
            nbpix2 = np.max(np.argwhere((xcol>=0)*(xcol<ncols))+2)
            ycol = yr+signy*Rows[az2,1:nbpix]
            nbpix2 = min(nbpix2,np.max(np.argwhere((ycol>=0)*(ycol<nrows))+2))  
            if nbpix>nbpix2:
                nbpix=nbpix2
                xcol = xr+signx*Cols[az2,1:nbpix]
                ycol = yr+signy*Rows[az2,1:nbpix]
            tp = np.argwhere(MNT_100[ycol,xcol]==-9999) 
            if tp.shape[0]>0:
                nbpix=np.min(np.argwhere(MNT_100[ycol,xcol]==-9999))  
                xcol = xr+signx*Cols[az2,1:nbpix]
                ycol = yr+signy*Rows[az2,1:nbpix]
            # plt.plot(Dist[az2,1:nbpix],MNT_100[ycol,xcol])
            # plt.title(str(az))
            # plt.show()
            Slope = (MNT_100[ycol,xcol]-MNT_100[yr,xr])/Dist[az2,1:nbpix]
            if Slope.shape[0]>0:
                slAz[i,az]=np.max(Slope)
   
    return slAz

def resample_raster(in_file_name,x,y,buff=300,save_file=False,out_file_name="",shp_proj=None):
    """
    Resample a raster
    ----------
    Parameters
    ----------
    in_file_name:    string      Complete name of the input raster
    out_file_name:   string      Complete name of the output raster
    newCsize:        int, float  Cell resolution of the output raster
    methode:         string      Method: gdal.GRA_Bilinear,gdal.GRA_Cubic,gdal.GRA_CubicSpline,gdal.GRA_NearestNeighbour

    ----------
    Examples
    --------
    >>> import gdal
    >>> resample_raster('mnt1m.tif','mnt5m.tif',5,methode=gdal.GRA_Bilinear)
    """        
    # Get info from source
    source_ds = gdal.Open(in_file_name)    
    driver = source_ds.GetDriver()
    src_proj = source_ds.GetProjection()
    dataset_val = source_ds.GetRasterBand(1)
    src_nodata = dataset_val.GetNoDataValue()
    if src_nodata is None :
        minval,maxval,meanval,sdt = source_ds.GetRasterBand(1).GetStatistics(0, 1)
        src_nodata = np.nan
        
    src_ncols = source_ds.RasterXSize
    src_nrows = source_ds.RasterYSize
    xmin,Csize_x,a,ymax,b,Csize_y = source_ds.GetGeoTransform()
    if max(Csize_x,Csize_y)<0.75:
        algo = "average"
    else:
        algo = 'bilinear'
    
    
    xmin,ymax = int(xmin+0.5),int(ymax+0.5)    
    Bandnb = source_ds.RasterCount    
    # Create ouptut raster
    xres=1.0
    yres=1.0
    xmin = int(x-buff-0.5)
    xmax = int(x+buff+0.5)
    ymin = int(y-buff-0.5)
    ymax = int(y+buff+0.5)
     
    target_ds = gdal.Warp('',source_ds,format='VRT',
                          outputType=gdal.GDT_Float32,
                          outputBounds=(xmin,ymin,xmax,ymax), 
                          xRes=1.0,yRes=1.0,srcNodata=src_nodata,dstNodata =-9999,
                          resampleAlg=algo)
    
    dataset_val = target_ds.GetRasterBand(1)
    Array = dataset_val.ReadAsArray()
    Array[np.isnan(Array)]=-9999
    if source_ds.GetRasterBand(1).GetNoDataValue() is not None:
        Array[Array==src_nodata]=-9999
    else:
        Array[Array<minval]=-9999
    Extent = [xmin,xmax,ymin,ymax]   
    target_ds = None 
    source_ds = None   
    
    nrows,ncols=Array.shape
    
    if np.min(Array)==np.max(Array):
        test=False
    else:
        test=True
    
        if save_file:
            ArrayToGtiff(Array,out_file_name,Extent,nrows,ncols,
                         1.0,shp_proj,-9999,raster_type='FLOAT32')
    
    return Extent,Array,test

def build_shape(Trouee_file,Csize,Extent,id_trouee,attribute_name="ID_OUV"):
    #Get raster dimension
    xmin,xmax,ymin,ymax = Extent[0],Extent[1],Extent[2],Extent[3]
    nrows,ncols = int((ymax-ymin)/float(Csize)+0.5),int((xmax-xmin)/float(Csize)+0.5)
    
    # Get information from source shapefile
    orig_data_source = ogr.Open(Trouee_file)
    
    #Get information from source shapefile    
    source_ds = ogr.GetDriverByName("Memory").CopyDataSource(orig_data_source, "")
    source_layer = source_ds.GetLayer()
    source_srs = source_layer.GetSpatialRef()
    source_type = source_layer.GetGeomType()
    
    #Save only the right feature 
    expression = '"ID_OUV" = '+str(id_trouee)
    source_layer_bis = source_ds.ExecuteSQL('SELECT * FROM '+str(source_layer.GetName())+' WHERE '+expression) 
    # Initialize the new memory raster
    driver = ogr.GetDriverByName('Memory')
    select_ds = driver.CreateDataSource("test")
    layerName = "test"
    layer = select_ds.CreateLayer(layerName, source_srs, source_type)
    layerDefinition = layer.GetLayerDefn()
    new_field = ogr.FieldDefn('ID_OUV', ogr.OFTInteger)
    layer.CreateField(new_field)
    ind=0
    for feat in source_layer_bis:
        geometry = feat.GetGeometryRef()
        feature = ogr.Feature(layerDefinition)
        feature.SetGeometry(geometry)
        feature.SetFID(ind)
        feature.SetField('ID_OUV',id_trouee)
        # Save feature
        layer.CreateFeature(feature)
        # Cleanup
        feature.Destroy()
        ind +=1
    # Cleanup
        
    # Initialize the new memory raster
    maskvalue = 1    
    xres=float(Csize)
    yres=float(Csize)
    geotransform=(xmin,xres,0,ymax,0, -yres)    
    target_ds = gdal.GetDriverByName('MEM').Create('', int(ncols), int(nrows), 1, gdal.GDT_Int32)
    target_ds.SetGeoTransform(geotransform)
    if source_srs:
        # Make the target raster have the same projection as the source
        target_ds.SetProjection(source_srs.ExportToWkt())
    else:
        # Source has no projection (needs GDAL >= 1.7.0 to work)
        target_ds.SetProjection('LOCAL_CS["arbitrary"]')
    # Rasterize
    err = gdal.RasterizeLayer(target_ds, [maskvalue], layer,options=["ATTRIBUTE=ID_OUV","ALL_TOUCHED=TRUE"])
    if err != 0:
        raise Exception("error rasterizing layer: %s" % err)
        target_ds.FlushCache()
        select_ds.Destroy()
    else:
        target_ds.FlushCache()
        mask_arr = target_ds.GetRasterBand(1).ReadAsArray()
        select_ds.Destroy()
        return np.uint8(mask_arr)    
        
def build_terrain_shape_buf(MNT_file,MNS_file,Trouee_file,Cent_i,idtrou,buff,mess):
    Csize=1.0
    x,y = Cent_i[idtrou,2:4]
    Extent,MNT,testMNT = resample_raster(MNT_file,x,y,buff)   
    Extent,MNS,testMNS = resample_raster(MNS_file,x,y,buff)   
    
    Shape = build_shape(Trouee_file,Csize,Extent,int(Cent_i[idtrou,0]))        
    Shape[Shape!=Cent_i[idtrou,0]]=0   
    
    if np.unique(MNT[Shape==Cent_i[idtrou,0]]).shape[0]==1:
        testMNT=False
    if np.unique(MNS[Shape==Cent_i[idtrou,0]]).shape[0]==1:
        testMNS=False      
           
    if not testMNT or not testMNS:
        Shape,Cent=None,None
        if not testMNT and testMNS:
            mess+="Le MNT fourni ne couvre pas l'ouverture n°"+str(int(Cent_i[idtrou,0]))+"\n"
        elif not testMNS and testMNT:
            mess+="Le MNS fourni ne couvre pas l'ouverture n°"+str(int(Cent_i[idtrou,0]))+"\n"
        else:
            mess+="Les MNT et MNS fournis ne couvrent pas l'ouverture n°"+str(int(Cent_i[idtrou,0]))+"\n"
            
    else:   
        Cent = np.zeros((1,Cent_i.shape[1]+1),dtype=np.float32)
        Cent[0,:-1]=Cent_i[idtrou]    
        Cent[0,-1]=np.mean(MNT[Shape==Cent_i[idtrou,0]])    
            
    return MNT,MNS,Shape,Extent,Cent,mess

def calc_sun_posi_terrain(Cent,slAz,idtrou,Month=6,Tlaps=1):  
    Lat,Lon,Elevation=Cent[idtrou,4:]
    slopeAz = slAz[idtrou]
    obs=ephem.Observer()
    obs.lat = str(Lat)
    obs.lon = str(Lon)  
    obs.elev = Elevation
    tf = timezonefinder.TimezoneFinder()  
    try:
        timezone_str = tf.certain_timezone_at(lat=Lat, lng=Lon)
        timezone = pytz.timezone(timezone_str)
    except:
        timezone_str = tf.certain_timezone_at(lat=45, lng=0)
        timezone = pytz.timezone(timezone_str)
    now = datetime.datetime.now(timezone)
    Year = now.year    
    ff,nbdays=monthrange(Year, Month)
    Sun_posi = np.ones((int(nbdays*24*60/Tlaps+1.5),2),dtype=np.int32)*-1
    i=0
    for day in range(1,nbdays+1):
        d1 = datetime.datetime(Year,Month,day,00,00,00)#Localtime
        obs.date = d1 - timezone.utcoffset(d1)
        try:
            sunrise=obs.next_rising(ephem.Sun()).datetime() 
            sunset=obs.next_setting(ephem.Sun()).datetime() 
        except:
            sunrise=datetime.datetime(Year,Month,day,00,00,00)
            sunset=datetime.datetime(Year,Month,day,23,59,00)
        t = sunrise
        while t<sunset:            
            obs.date = t
            sun=ephem.Sun(obs)
            azimut = int(math.degrees(sun.az)+0.5) 
            slope = math.tan(sun.alt)
            if slope>=slopeAz[azimut]:
                Sun_posi[i,0] = azimut
                Sun_posi[i,1] = abs(int(slope*10000+0.5))
                i+=1
            t+=datetime.timedelta(minutes=Tlaps)          
    Sun_posi=Sun_posi[:i]
    u,counts = np.unique(Sun_posi,axis=0,return_counts=True)
    Sun_posi = np.zeros((u.shape[0],3),dtype=np.int32)
    Sun_posi[:,0:2]=u
    Sun_posi[:,2]=counts*Tlaps      
    az_list,indmin = np.unique(Sun_posi[:,0],return_index=True)
    Az_val = np.zeros((az_list.shape[0],2),dtype=np.int32)
    Az_val[:,0] = az_list
    Az_val[:,1] = indmin
    
    return Sun_posi,Az_val,nbdays

def get_trouee_carac(Sun_Dur,MNT,MNS):
    Pente = cd.pente(MNT,1.0,-9999)
    tp = ((Sun_Dur!=-1)*(MNT!=-9999))>0
    Dtm_sl = int(np.mean(Pente[tp]))
    Expo = cd.exposition(MNT,1.0,-9999)
    Dtm_az_val = Expo[tp]
    Dtm_pol = np.radians(np.mod(360-(Dtm_az_val),360))       
    x = np.mean(np.cos(Dtm_pol))
    y = np.mean(np.sin(Dtm_pol))
    Dtm_az = int((math.degrees(math.atan2(x,y))-90)%360)      
        
    Dist = cd.calcul_distance_de_cout(np.int8(Sun_Dur!=-1),
                                      1.0,Max_distance=15)
    
    MNH = np.maximum(MNS,MNT)-np.minimum(MNS,MNT)
    tp = ((Dist>0)*(MNT!=-9999)*(MNS!=-9999))>0
    
    Stand_H = round(np.mean(MNH[tp]),1)
    Stand_Hmax = round(np.max(MNH[tp]),1)
    return Dtm_sl,Dtm_az,Stand_H,Stand_Hmax,np.sum(Sun_Dur!=-1)

def plot_Sun_Duration_terrain(Sun_Dur,MNT,Dtm_sl,Dtm_az,Stand_H,Stand_Hmax,
                              str_title,fig_name=None,borne_sup=[1,2,4,6]):     
    
    Dtm_masked = np.ma.masked_array(np.copy(MNT),Sun_Dur!=-1)   
    Csize =1.0
        
    #zoom 
    inds = np.argwhere(Sun_Dur>=0)
    ymin = int(min(np.min(inds[:,0]),np.min(inds[:,1]))-10/Csize)
    xmin = ymin
    ymax = int(max(np.max(inds[:,0]),np.max(inds[:,1]))+10/Csize+0.5)
    xmax=ymax    
    
    X, Y = np.meshgrid(np.arange(xmin, xmax, dtype=np.int), 
                       np.arange(ymin, ymax, dtype=np.int))
    
    Z = Dtm_masked[ymin:ymax,xmin:xmax]    
    contourval = int((np.max(Z)-np.min(Z))/5+0.5)
        
    #Colors used
    cmap = ListedColormap(["gainsboro","#0072B2",
                           "#56B4E9","#009E73",
                           "#F0E442","#E69F00",
                           "#CC79A7"])    
     
    # Define a normalization from values -> colors
    nbcol = len(borne_sup)+2
    bins = np.empty((nbcol,))
    bins[0:2]=-0.5,0.000000000001
    i=2
    while i<bins.shape[0]:
        bins[i]=borne_sup[i-2]
        i+=1
    
    inds = np.digitize(Sun_Dur, bins)    
    bound_col=[0]
    for i in range(nbcol+1):
        bound_col.append(i+0.5)        
    norm = colors.BoundaryNorm(bound_col, nbcol+1)
    
    # #Text on clear cut dimension  
    str_Tarea ="Hauteur moyenne en lisière : "+str(Stand_H)+ " m\n"
    str_Tarea+="Hauteur dominante en lisière : "+str(Stand_Hmax)+ " m\n"
    # # Give dimension accroding to StandH
    # if int(f_dim1/Stand_H)==f_dim1/Stand_H:
    #     st1 = str(int(f_dim1/Stand_H))+"H"
    # else:
    #     st1 = str(round(f_dim1/Stand_H,2))+"H"
    # if int(f_dim2/Stand_H)==f_dim2/Stand_H:
    #     st2 = str(int(f_dim2/Stand_H))+"H"
    # else:
    #     st2 = str(round(f_dim2/Stand_H,2)) +"H"     
    # ##################################################################
    # if f_dim1 != f_dim2:
    #     str_Tarea+="Dimension de la trouée : "+st1+ r'$ \times $'+st2+'\n'
    # else:
    #     str_Tarea+="Dimension de la trouée : "+st1+ '\n'
    
    # if form=='rectangle':
    #     surf = str(int(f_dim1*f_dim2+0.5))        
    # else:
    #     surf = str(int(0.5*f_dim1*0.5*f_dim2*math.pi+0.5))
    surf = str(np.sum(Sun_Dur!=-1))
    str_Tarea+="Surface de l'ouverture : "+surf+ ' m\u00B2'
    
    #Legend title
    str_title_ld = r"$\bf{Durée}$"+" "+r"$\bf{d}$"+"'"
    str_title_ld +=  r"$\bf{ensoleillement}$"+" "
    str_title_ld +=  r"$\bf{direct en juin}$" + " "  
    str_title_ld +=  r"$\bf{(\%}$" + " "+r"$\bf{de}$" + " "
    str_title_ld +=  r"$\bf{la}$" + " "+r"$\bf{surface)}$" 
    
    # Calc_area
    Tarea_norm = np.sum(inds>0)
    area_list = []
    for val in range(1,nbcol+1): 
        if val==1:
            txt = "0 h/jour : "
        elif val==2:
            txt = 'Moins de '+str(borne_sup[0])+" h/jour : "
        elif val>2 and val<nbcol:
            txt = 'Entre '+str(borne_sup[val-3])+' et '+str(borne_sup[val-2])+" h/jour : "
        else:
            txt = "Plus de "+str(borne_sup[val-3])+" h/jour : "        
        txt += str(int(np.sum(inds==val)/Tarea_norm*100+0.5))+ ' %'
        area_list.append(txt)
        
    #Start fig
    plt.ioff()
    fig, ax = plt.subplots(figsize=(11.14,7.9),dpi=100)
          
    ax.imshow(inds,cmap=cmap,norm=norm,interpolation=None)
    ##add contour
    if contourval>0:      
        ax.contour(X, Y, Z, contourval, colors='black', linestyles=':',linewidths=0.75,alpha=0.7)    
    
    #legend
    legend_labels = {"#0072B2": area_list[0], 
                     "#56B4E9": area_list[1], 
                     "#009E73": area_list[2],
                     "#F0E442": area_list[3],
                     "#E69F00": area_list[4],
                     "#CC79A7": area_list[5]}
    patches = [Patch(color=color, label=label)
               for color, label in legend_labels.items()]
    
    fig.legend(loc="lower center",handles=patches,
               borderaxespad=0.05,frameon=False,
               title=str_title_ld,ncol=2)
    plt.subplots_adjust(bottom=0.11)  
    #ax.add_collection(lc)
    if Dtm_sl>0:
        #Slope arrow
        bbox_props = dict(boxstyle="rarrow", fc="#9a1010", ec="#9a1010", lw=2,alpha=0.8)
        ax.text(0.1, 0.90, "          ", ha="center", va="center",
                rotation=conv_az_to_polar(Dtm_az),
                size=15,color='white',transform=ax.transAxes,
                bbox=bbox_props) 
        ang = conv_az_to_polar(Dtm_az)
        if Dtm_az>180:
            ang -= 180
        ax.text(0.11, 0.90, " Pente ", ha="center", va="center",
                rotation=ang,
                size=15,color='white',transform=ax.transAxes)
        #slope value
        ax.text(0.24, 0.90, str(Dtm_sl)+' %', ha="center", va="center",
                size=15,color='#9a1010',transform=ax.transAxes)
    else:
        ax.text(0.15, 0.95,"Pente : "+ str(Dtm_sl)+' %', ha="center", va="center",
                size=15,color='#9a1010',transform=ax.transAxes)
    #North arrow
    bbox_props = dict(boxstyle="rarrow", fc=(0.7, 0.7, 0.7), ec="black", lw=2)
    ax.text(0.95, 0.92, "       ", ha="center", va="center",
            rotation=90,transform=ax.transAxes,
            size=15,
            bbox=bbox_props)
    ax.text(0.95, 0.92, " N ", ha="center", va="center",transform=ax.transAxes,           
            size=15)     
    plt.xlim(xmin, xmax)
    plt.ylim(ymax, ymin)
    ax.text(0.01, 0.01, str_Tarea, ha="left", va="bottom",            
             size=12,transform=ax.transAxes)    
    scalebar = ScaleBar(Csize,frameon=False,location="lower right")
    plt.gca().add_artist(scalebar)
    ax.set_axis_off()       
    plt.gca().set_aspect('equal', adjustable='box')
    plt.title(str_title, fontsize=20,fontweight='bold')
    if fig_name is not None:
        fig.savefig(fig_name, dpi=fig.dpi,bbox_inches='tight')
    #plt.show() 
    plt.close()
    
        
def check_field(filename,fieldname):    
    test=0
    source_ds = ogr.Open(filename)
    layer = source_ds.GetLayer()    
    ldefn = layer.GetLayerDefn()
    for n in range(ldefn.GetFieldCount()):
        fdefn = ldefn.GetFieldDefn(n)
        if fdefn.name==fieldname:
            test=1
            break
    if test:
        featureCount = layer.GetFeatureCount()
        vals = []
        for feat in layer:
            val = feat.GetField(fieldname)
            if val is not None:
                vals.append(feat.GetField(fieldname))
        source_ds.Destroy() 
        if len(vals)!=featureCount:
            test=2
    return test,vals

def raster_get_info(in_file_name):
    source_ds = gdal.Open(in_file_name)    
    src_proj = osr.SpatialReference(wkt=source_ds.GetProjection())
    src_ncols = source_ds.RasterXSize
    src_nrows = source_ds.RasterYSize
    xmin,Csize_x,a,ymax,b,Csize_y = source_ds.GetGeoTransform()
    ymin = ymax+src_nrows*Csize_y
    nodata = source_ds.GetRasterBand(1).GetNoDataValue()
    names = ['ncols', 'nrows', 'xllcorner', 'yllcorner', 'cellsize','NODATA_value']
    values = [src_ncols,src_nrows,xmin,ymin,Csize_x,nodata]
    Extent = [xmin,xmin+src_ncols*Csize_x,ymin,ymax]
    return names,values,src_proj,Extent

def check_files(MNT_file,MNS_file,Trouee_file):
    test = 1
    Csize = None
    mess="\nLES PROBLEMES SUIVANTS ONT ETE IDENTIFIES CONCERNANT LES ENTREES SPATIALES: \n"
    
    #Check DTM    
    try:
        names,values,proj,Extent = raster_get_info(MNT_file)  
        Csize = values[4]
        if values[5]==None:           
            mess+=" -   Raster MNT : Aucune valeur de NoData definie. Attention, cela peut engendrer des résultats éronnés.\n" 
    except:
        test=0
        mess+=" -   Raster MNT :  Le chemin d'acces est manquant ou incorrect. Ce raster est obligatoire\n" 
    
    #Check DSM    
    try:
        names,values,proj,Extent = raster_get_info(MNS_file)  
        Csize = values[4]
        if values[5]==None:           
            mess+=" -   Raster MNS : Aucune valeur de NoData definie. Attention, cela peut engendrer des résultats éronnés.\n" 
    except:
        test=0
        mess+=" -   Raster MNS :  Le chemin d'acces est manquant ou incorrect. Ce raster est obligatoire\n" 
        
    #Check Waypoints 
    try:    
        testfd,vals = check_field(Trouee_file,"ID_OUV")
        if testfd==0:
            test=0
            mess+=" -  Couche shapefile des ouvertures : Le champs 'ID_OUV' est manquant\n"  
        elif testfd==2:
            test=0
            mess+=" -  Couche shapefile des ouvretures : Veuillez remplir le champs 'ID_OUV' pour toutes les entités\n" 
        else:
            if np.min(vals)<=0:
                test=0
                mess+=" -  Couche shapefile des ouvertures : Les valeurs de 'ID_OUV' doivent être > 0\n" 
        
        testfd,vals =  check_field(Trouee_file,"EXIST")
        if testfd==0:
            test=0
            mess+=" -  Couche shapefile des ouvertures : Le champs 'EXIST' est manquant\n"  
        elif testfd==2:
            test=0
            mess+=" -  Couche shapefile des ouvertures : Veuillez remplir le champs 'EXIST' pour toutes les entités\n"    
        else:
            if np.min(vals)<0 or np.max(vals)>1:
                test=0
            mess+=" -  Couche shapefile des ouvertures : Le champs 'EXIST' ne peut prendre que les valeurs 0 (Non existant) ou 1 (Existant)\n"    
                        
    except:
        test=0
        mess+=" -   Couche shapefile des ouvertures : Le chemin d'acces est manquant ou incorrect. Cette couche est obligatoire\n" 
    
    return test,mess,Csize
  
def save_param_file(MNT_file,MNS_file,Trouee_file,ResFold):
    param = []
    param.append([MNT_file,MNS_file,Trouee_file,ResFold])
    param = np.array(param)
    np.save(ResFold+"LUMIFOR_terrain_param.npy",param)
  
    
def process_calc_terrain(MNT_file,MNS_file,Trouee_file,ResFold,
                         save_sig=True,save_fig=True,
                         buff=300,borne_sup=[1,2,4,6]):    
    
    
    test,mess,Csize = check_files(MNT_file,MNS_file,Trouee_file)
    save_param_file(MNT_file,MNS_file,Trouee_file,ResFold)
    
    if not test: 
        nbtrou=0
        Res = np.zeros((nbtrou,18),dtype=np.float32)
    
    else:       
        RSig = ResFold+"/SIG/"
        if os.path.exists(RSig):
            shutil.rmtree(RSig)    
        try:os.mkdir(RSig)
        except: pass   
    
        RFig = ResFold+"/Figures/"
        if os.path.exists(RFig):
            shutil.rmtree(RFig)    
        try:os.mkdir(RFig)
        except: pass 
    
        Csize = 1.0   
        strCsize='1'
          
        Rows,Cols,Dist,Nbpix,MNT_100,Ext_100 = load_Azimuts(strCsize,Csize)
        
        #Get centroids and calculat terrain mask
        Cent_i = get_centroids(Trouee_file)
        slAz = get_slope_az(Cent_i,Ext_100,MNT_100)    
        shp_proj,proj = get_proj_from_road_network(Trouee_file)
        
        #Process Sun duration
        nbtrou = Cent_i.shape[0]
        Res = np.zeros((nbtrou,18),dtype=np.float32)
        #idtrouee exist x y lat lon elev Pente Azi H Ho Surf 0h 0-1h 1-2h 2-4h 4-6h >6h
        #0        1     2 3 4   5   6    7     8   9 10 11   12 13   14   15   16    17
        
        mess = "Calculs terminés\n\n"
        for i in range(nbtrou):
            MNT,MNS,Shape,Extent,Cent,mess = build_terrain_shape_buf(MNT_file,MNS_file,
                                                                     Trouee_file,
                                                                     Cent_i,i,buff,mess)
            if Shape is None:            
                continue
            
            Res[i,0:7]=Cent
            
            nrows,ncols=MNT.shape    
            Sun_duration = np.ones_like(MNS,dtype=np.int32)*-1    
            Sun_posi,Az_val,nbdays = calc_sun_posi_terrain(Cent,slAz,0)
            hole_list = np.int32(np.argwhere(Shape==Cent[0,0]))
            MNS_used=np.copy(MNS)
            if not Cent[0,1]:
                MNS_used[Shape==Cent[0,0]]=np.minimum(MNT[Shape==Cent[0,0]],MNS_used[Shape==Cent[0,0]])
            Sun_duration = cd.sun_duration_real_terrain(hole_list,MNS_used,Sun_posi,
                                                     Az_val,Rows,Cols, Dist,Nbpix,
                                                     Sun_duration)
    
            Sun_duration_hj2 = Sun_duration/(nbdays*60)
            Sun_duration_hj2[Shape==0]=-1
            
            Dtm_sl,Dtm_az,Stand_H,Stand_Hmax,Surf = get_trouee_carac(Sun_duration_hj2,MNT,MNS)
            Res[i,7:12]=Dtm_sl,int(Dtm_az*20/18+0.5),Stand_H,Stand_Hmax,Surf        
            
            Surface = calc_surface_min(Sun_duration,np.int8(Shape>0),nbdays)
            tab_plage = calc_surface_plage(Surface,borne_sup)
            Res[i,12:]=np.round(tab_plage[:,2],1)
            
            if save_sig:        
                filename = RSig+"Ouverture_"+str(int(Cent[0,0]))
                ArrayToGtiff(Sun_duration_hj2,filename,Extent,nrows,ncols,Csize,shp_proj,-1,raster_type='FLOAT32')
            if save_fig:
                str_title = "Ouverture "+str(int(Cent[0,0]))
                filename = RFig+"Ouverture_"+str(int(Cent[0,0]))+".png"
                plot_Sun_Duration_terrain(Sun_duration_hj2,MNT,Dtm_sl,Dtm_az,Stand_H,Stand_Hmax,
                                          str_title,fig_name=filename,borne_sup=[1,2,4,6])
            
        head_text="ID_OUV;EXIST;X_cen [m];Y_cen [m];Lat_cen [°];Lon_cen [°];"
        head_text+="Altitude [m];Pente [%];Exposition [gr];Hmoy [m];Ho [m];"
        head_text+="Surface [m²];"  
        head_text+="0 h/jour;Moins de "+str(borne_sup[0])+" h/jour;"
        prev = "Entre "+str(borne_sup[0])+ ' et '
        for duree in borne_sup[1:]:
            head_text += prev + str(duree)+" h/jour;"
            prev = "Entre "+str(duree)+ ' et '
        head_text += "Plus de "+str(duree)+" h/jour"
        
        filename = ResFold+"Recapitulatif_calcul.csv"
        
        fmt = "%i","%i",'%1.1f','%1.1f','%1.4f','%1.4f','%i','%i','%i','%1.1f','%1.1f','%i','%1.1f','%1.1f','%1.1f','%1.1f','%1.1f','%1.1f'
        
        np.savetxt(filename, Res, fmt=fmt, delimiter=';',header=head_text)    
    return Res,mess,nbtrou