# -*- coding: utf8 -*-
"""
Software: LUMIFOR
File: Lumifor_0_param_optimisation_ouverture.py
Copyright (C) Sylvain DUPIRE 2023
Authors: Sylvain DUPIRE
Contact: sylvain.dupire@inrae.fr
Version: 1.1
Date: 2023/04/27
License :  GNU-GPL V3
"""

import sys,traceback,os,shutil,time
from Lumifor_1_function import *
from distutils.dir_util import copy_tree

###############################################################################
### Parametres
###############################################################################
Opt_Rspace = "F:/SylvaLab/Projets/ONF/Lumifor/Scripts/Results/" #Dossier de résultats

############################
### Position
############################
Lat = 45.4290     # °    Latitude
Lon = 5.979187    # °    Longitude
Elevation = 343   # m    Altitude

###########################
### Parametre du versant
############################
Dtm_sl = 10                # %          Pente du versant
Dtm_az = 0              # grades     Exposition du versant

############################
### Parametre du peuplement
############################
Stand_H = 24               # m          Hauteur du peuplement

############################
### Plage à favoriser
############################
# Valeurs possible pour les bornes min (Plage_min) et max (Plage_max):
    # 0 : 0h/jour
    # 1 : 1h/jour
    # 2 : 2h/jour
    # 3 : 4h/jour
    # 4 : 6h/jour
    # 5 : 24h/jour
Plage_min = 0       # Borne minimale de la plage à favoriser   
min_inc = False     # Inclure strictement la borne min dans la plage à favoriser  
Plage_max = 2       # Borne maximale de la plage à favoriser 
max_inc = False     # Inclure strictement la borne max dans la plage à favoriser  

############################
### Plage à éviter
############################
# Valeurs possible pour la duree max (Durmax):
    # 0 : 0h/jour
    # 1 : 1h/jour
    # 2 : 2h/jour
    # 3 : 4h/jour
    # 4 : 6h/jour
Durmax = 3      # Duree max d'ensoleillement
SurfDmax = 10   # %    Sur une proportion de x% de la surface de l'ouverture

############################
### Foret de protection
############################
Fprotect = False   # True ou False      Prendre en compte le critère forêt protection
LProtect = 25      # m                  Longueur max de l'ouverture dans le sens
                   #                    de plus grande pente  
                   #                    (pris en compte seulement si Fprotect=True)
                   
############################
### Parametres de calculs
############################
Tlaps = 1           # min  Pas d'analyse 
Csize = 1           # m    Resolution du pixel
borne_sup = [1,2,4,6]

###############################################################################
### Process
###############################################################################
DB_form,DB_Dtm_az,DB_Dtm_sl,DB_Stand_H,DB_dim1,DB_dim2, \
      DB_f_az,DB_Lat,DB_Lpente,DB_Surf,Radiations,Clouds=load_Database()    
      
tree = spatial.cKDTree(Clouds[:,:2]/100.)      
       
if not Fprotect:
    LProtec = 100*Stand_H

Plage_min = min(Plage_max,Plage_min)
Plage_max = max(Plage_max,Plage_min)
Durmax = min(Plage_max+1,4)

if Plage_max==5:
    SurfDmax = 100
            
Rspace_O,exist = create_res_dir_opti(Opt_Rspace,Dtm_az,Dtm_sl,Elevation,
                                    Stand_H,Lat,Lon,Plage_min,Plage_max ,
                                    Durmax ,SurfDmax,LProtec)
try:            
    #calc radiation
    plot_rad_scale_v(Radiations,Lat,Lon,Elevation,Dtm_az,Dtm_sl,
                     Clouds,tree, Rspace_O+"Rayonnement.png",col=3)
    
    #get best forms
    Temp_Rspace = Opt_Rspace + "Temp/"
    Tab_Best,mess = process_best_form(DB_form,DB_Dtm_az,DB_Dtm_sl,
                                      DB_Stand_H,DB_dim1,DB_dim2,
                                      DB_f_az,DB_Lat,DB_Lpente,DB_Surf,
                                      Lat,Lon,Elevation,Tlaps,Csize,
                                      Rspace_O,Dtm_az,Dtm_sl,Stand_H,
                                      LProtec,Plage_min,Plage_max,
                                      Durmax,min_inc,max_inc,SurfDmax,Temp_Rspace)
    
    shutil.rmtree(Temp_Rspace)   
    print("Calcul terminé")   
    print(mess)   
    
except Exception:    
    log = open(Opt_Rspace + 'error_log.txt', 'w')
    traceback.print_exc(file=log)
    log.close()              
    
try:   
    if len(Rspace_O)>0:
        filename = Rspace_O+"Resultat_Optimisation_LUMIFOR.xlsx"
        save_xls(filename,Rspace_O)
        print('Résultats sauvargardés') 
    else:
        Mess = "Un calcul est nécessaire avant de sauvegarder les résultats"
        print(Mess)  
        
except Exception:
    log = open(Opt_Rspace + 'error_log.txt', 'w')
    traceback.print_exc(file=log)
    log.close()